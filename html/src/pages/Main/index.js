import React from 'react';
import { connect } from 'react-redux';
import { imageList } from './../../constants/Import_Images';
import { setValues } from '../../store/redux';
import styled from 'styled-components';
//features
import VendingMachine from '../../features/VendingMachine';
import LoginPage from '../../features/Login';
//component
import {
  HistoryModal,
  H2PModal,
  LoadingModal,
  RewardModal,
  MessageModal,
  PolicyModal,
  WarningModal,
} from '../../features/modal';

class Main extends React.Component {
  constructor(props){
    super(props);
    this.state = {

    }
  }
  render() {
    return (
        <Layout>
            <RatioPreserver>
                {
                    this.props.jwtToken && this.props.jwtToken !== ''
                  ?
                      <>
                          <Decor className="left" src={imageList['decor_left']} alt='' />
                          <Decor className="right" src={imageList['decor_right']} alt='' />
                          <MachineWrapper>
                              <VendingMachine></VendingMachine>
                          </MachineWrapper>
                      </>

                  :
                      <LoginPage/>

                }
                <Footer>
                  <img className='left' src={imageList['footer_left']} alt=""/>
                  <img className='right' src={imageList['footer_right']} alt=""/>
              </Footer>
            </RatioPreserver>
            <HistoryModal/>
            <H2PModal/>
            <LoadingModal/>
            <RewardModal/>
            <MessageModal/>
            <PolicyModal/>
            <WarningModal/>
        </Layout>
    );
  }
}

const mapStateToProps = state => ({ ...state.Main });

const mapDispatchToProps = { setValues };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);

const Layout  = styled.div`
    background: top center no-repeat url(${imageList['bg_main']});
    background-size: cover;
    width: 100vw;
    height: 100vh;
    background-size: cover;
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
`
const RatioPreserver = styled.div`
    position: relative;
    display: inline-block;
    width: 100vw;
    height: 100vh;
    max-width: calc(100vh / 9 * 16);
    max-height: calc(100vw / 16 * 9);
    margin: 0 auto;
`
const MachineWrapper = styled.div`
    width: 57%;
    height: 92%;
    margin: 2% auto;
`
const Decor  = styled.img`
    position: absolute;
    bottom: 0;
    pointer-events: none;
    &.left{
      left: 0;
      max-width: 25%;
    }
    &.right{
      right: 0;
      max-width: 23%;
    }
`
const Footer = styled.div`
  position: absolute;
  bottom: 1.5%;
  left: 0;
  right: 0;
  width: 97%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 0 auto;
  .left{
    max-width: 18%;
  }
  .right{
    max-width: 15%;
  }
`
