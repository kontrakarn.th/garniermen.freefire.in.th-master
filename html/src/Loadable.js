import React from 'react';
import Loadable from 'react-loadable';

import { Loader } from './common/Loader';

const LoadingComponent = props => {
  if (props.error) {
    return <div>Error!</div>;
  } else if (props.timedOut) {
    return <div>Taking a long time...</div>;
  } else if (props.pastDelay) {
    return <Loader />;
  } else {
    return null;
  }
};

export const HomeLoadable = Loadable({
  loader: () => import(/* webpackChunkName: "home" */ './pages/Main/index'),
  loading: LoadingComponent,
  timeout: 5000 // 5 s
  // delay: 0 // .3 s
});