import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';
// import Main from './pages/main';

import configStore from './ConfigStore';
// Route Middleware
import {HomeLoadable} from './Loadable';
import './styles/index.scss';

import OauthMiddleware from './middleware/OauthMiddleware';

const { store } = configStore();

const Routes = () => (
  <Provider store={store}>
      <Router>
        <div>
          <Route
            path={process.env.REACT_APP_EVENT_PATH}
            component={OauthMiddleware}
          />
          <Route
            path={process.env.REACT_APP_EVENT_PATH}
            exact
            component={HomeLoadable}
          />
        </div>
      </Router>
  </Provider>
);

export default Routes;
