import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

// logger
import { logger } from './utils/logger';
// Redux-promise middleware
import promise from 'redux-promise-middleware';
// Redux Reducers
import { MainReducer } from './store/redux';

const middleware = [thunk, logger, promise];

const persistedReducer = 
  combineReducers({
    Main: MainReducer,
  })

export default () => {
  let store = createStore(persistedReducer, applyMiddleware(...middleware));
  return { store };
};
