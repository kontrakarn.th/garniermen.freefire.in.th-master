const bg_machine = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/bg_machine.png';
const bg_main = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/bg_main.jpg';
const bg_modal = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/bg_modal.jpg';
const btn_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/btn_confirm.jpg';
const btn_fb = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/btn_fb.png';
const btn_gg = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/btn_gg.png';
const btn_vk = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/btn_vk.png';
const content_item_left = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/content_item_left.png';
const content_item_right = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/content_item_right.png';
const decor_left = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/decor_left.png';
const decor_right = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/decor_right.png';
const footer_left = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/footer_left.png';
const footer_right = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/footer_right.png';
const logo_event = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/logo_event.png';
const logo = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/logo.png';
const bg_countdown = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/bg_countdown.png';

const btn_h2p = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/btn_h2p.png';
const btn_logout = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/btn_logout.png';
const btn_history = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/btn_history.png';
const frame_inner = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/frame_inner.png';
const bg_dragon = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/bg_dragon.png';
const bg_eff = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/bg_eff.png';
const text_itemcode = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/text_itemcode.png';
const bg_inputbox = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/bg_inputbox.png';
const token = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/token.png';
const btn_close = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/btn_close.png';
const modal_warning = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/modal_warning.jpg';
const item_history = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/item_history.png';
const title_reward = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/title_reward.png';
const eff_dragon = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/eff_dragon.png';
const eff_ring = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/eff_ring.png';
const fs_token = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/fs_token.png';
const item_name = 'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/item_name.png';

export const imageList = {
    bg_machine,
    bg_main,
    bg_modal,
    btn_confirm,
    btn_fb,
    btn_gg,
    btn_vk,
    content_item_left,
    content_item_right,
    decor_left,
    decor_right,
    footer_left,
    footer_right,
    logo_event,
    logo,
    btn_h2p,
    btn_logout,
    btn_history,
    frame_inner,
    bg_dragon,
    bg_countdown,
    bg_eff,
    text_itemcode,
    bg_inputbox,
    token,
    btn_close,
    modal_warning,
    item_history,
    title_reward,
    eff_dragon,
    eff_ring,
    fs_token,
    item_name,
};
