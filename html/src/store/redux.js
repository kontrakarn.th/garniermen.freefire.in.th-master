import { fetch } from 'cross-fetch';

const SET_JWT_TOKEN = 'SET_JWT_TOKEN';
const SET_LOGIN_URL = 'SET_LOGIN_URL';
const SET_LOGIN_URL_FB = 'SET_LOGIN_URL_FB';
const SET_LOGOUT_URL = 'SET_LOGOUT_URL';
const JWT = {
  GET_JWT: 'GET_JWT',
  GET_JWT_PENDING: 'GET_JWT_PENDING',
  GET_JWT_FULFILLED: 'GET_JWT_FULFILLED',
  GET_JWT_REJECTED: 'GET_JWT_REJECTED'
};
const GET_HOME_INFO = {
  GET_HOME_INFO: 'GET_HOME_INFO',
  GET_HOME_INFO_PENDING: 'GET_HOME_INFO_PENDING',
  GET_HOME_INFO_FULFILLED: 'GET_HOME_INFO_FULFILLED',
  GET_HOME_INFO_REJECTED: 'GET_HOME_INFO_REJECTED'
};
const REDEEM = {
  GET_REDEEM: 'GET_REDEEM',
  GET_REDEEM_PENDING: 'GET_REDEEM_PENDING',
  GET_REDEEM_FULFILLED: 'GET_REDEEM_FULFILLED',
  GET_REDEEM_REJECTED: 'GET_REDEEM_REJECTED'
};
const HISTORY = {
  GET_HISTORY: 'GET_HISTORY',
  GET_HISTORY_PENDING: 'GET_HISTORY_PENDING',
  GET_HISTORY_FULFILLED: 'GET_HISTORY_FULFILLED',
  GET_HISTORY_REJECTED: 'GET_HISTORY_REJECTED'
};

const SET_AUTH_STATE = 'SET_AUTH_STATE';

const initialState = {
  start: false,
  playing: false,
  policy_stat: false,
  // animation
  authState: null,
  jwtToken: null,
  loading: false,
  error: false,
  loginUrl: null,
  logoutUrl: null,
  modal_open: "",
  modal_message: "",
  modal_type:"",
  item_get: [],
  account_id:0,
  account_name:"",
  end_at:"0000-00-00 00:00:00",
  gems: 0,
  history:[],
  redeem_code:"",
  play_animation:false,
  ban:true,
};

export const MainReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_AUTH_STATE:
      return { ...state, authState: action.value };
    case SET_JWT_TOKEN:
      return { ...state, jwtToken: action.value };
    case JWT.GET_JWT_PENDING:
      return { ...state, loading: true };
    case JWT.GET_JWT_FULFILLED:
      return { ...state, jwtToken: action.payload, loading: false };
    case JWT.GET_JWT_REJECTED:
      return {
        ...state,
        error: true,
        jwtToken: action.payload,
        loading: false
      };
  case GET_HOME_INFO.GET_HOME_INFO_PENDING:
    return {
      ...state,
      loading:true,
      modal_open: "loading"
    };
  case GET_HOME_INFO.GET_HOME_INFO_FULFILLED:
    if (action.payload.data) {
        if(action.payload.status==true){
            return {
              ...state,
              ...action.payload.data,
              loading:false,
              modal_open: "policy"
            };
        }else{
            return {
              ...state,
              ...action.payload.data,
              loading:false,
              modal_message: action.payload.message,
              modal_open: "message"
            };
        }

    } else {
      return {
        ...state,
        ...action.payload.data,
        loading:false,
        modal_message: action.payload.message,
        modal_open: "message"
      };
    }
  case GET_HOME_INFO.GET_HOME_INFO_REJECTED:
    return {
      ...state,
      loading:false,
      error: true,
      modal_message: "เกิดข้อผิดพลาด",
      modal_open: "message"
    };
  case REDEEM.GET_REDEEM_PENDING:
    return {
      ...state,
      loading:true,
      modal_open: "loading",
      item_get:[],
    };
  case REDEEM.GET_REDEEM_FULFILLED:
    if (action.payload.data) {
      return {
        ...state,
        ...action.payload.data,
        loading:false,
        play_animation:true,
        modal_open: "",
        history:[]
      };
    } else {
      return {
        ...state,
        ...action.payload.data,
        loading:false,
        modal_message: action.payload.message,
        modal_open: "message",
        redeem_code:"",
      };
    }
  case REDEEM.GET_REDEEM_REJECTED:
    return {
      ...state,
      loading:false,
      error: true,
      modal_message: "เกิดข้อผิดพลาด",
      modal_open: "message",
      redeem_code:""
    };
  case HISTORY.GET_HISTORY_PENDING:
    return {
      ...state,
      loading:true,
      modal_open: "loading"
    };
  case HISTORY.GET_HISTORY_FULFILLED:
    if (action.payload.data) {
      return {
        ...state,
        ...action.payload.data,
        loading:false,
        modal_open: "history"
      };
    } else {
      return {
        ...state,
        ...action.payload.data,
        loading:false,
        modal_message: action.payload.message,
        modal_open: "message"
      };
    }
  case HISTORY.GET_HISTORY_REJECTED:
    return {
      ...state,
      loading:false,
      error: true,
      modal_message: "เกิดข้อผิดพลาด",
      modal_open: "message"
    };
    case SET_LOGIN_URL:
      return { ...state, loginUrl: action.value };
    case SET_LOGIN_URL_FB:
      return { ...state, loginUrlFB: action.value };
    case SET_LOGOUT_URL:
      return { ...state, logoutUrl: action.value };
    case "SET_VALUES": return { ...state, ...action.value };
    case "CLOSE_MODAL": return {...state,modal_open:''};
    default:
      return state;
  }
};

export const setAuthState = value => ({
  type: SET_AUTH_STATE,
  value
});

export const setJwtToken = value => ({
  type: SET_JWT_TOKEN,
  value
});

export const setLoginUrl = value => ({
  type: SET_LOGIN_URL,
  value
});

export const setLoginUrlFB = value => ({
  type: SET_LOGIN_URL_FB,
  value
});

export const setLogoutUrl = value => ({
  type: SET_LOGOUT_URL,
  value
});

export const setValues = (value) =>{
    // console.log(value)
    return ({
        type: "SET_VALUES",
        value
    })
};

export const modalClose =()=>({type: "CLOSE_MODAL"})

export const getJwtToken = data => {
  const url = `${process.env.REACT_APP_API_SERVER_HOST +
    process.env.REACT_APP_API_AUTH}?access_token=${data.oauthCookie}&sTicket=${
    data.sTicketCookie
    }`;
  return {
    type: JWT.GET_JWT,
    payload: fetch(url).then(response => response.json()).then(
      result => {
        if (result && result.status === true) {
          if (result.data.token !== '') {
            return result.data.token;
          } else {
            return null;
          }
        } else {
          return null;
        }
      },
      error => console.log('Error API : ', error)
    )
  };
};

export const getHomeInfo = (jwtToken) => {
  const url = process.env.REACT_APP_API_SERVER_HOST + process.env.REACT_APP_API_POST_GET_HOME_INFO
  return {
    type: GET_HOME_INFO.GET_HOME_INFO,
    payload: fetch(url, {
      method: 'post',
      headers: {
        'Authorization': 'Bearer ' + jwtToken
      }
    }).then(response => response.json()).then(
      result => {
          return result;
      },
      error => console.log('Error API : ', error)
    )
  };
};

export const goRedeem = (data) => {
  const url = process.env.REACT_APP_API_SERVER_HOST + process.env.REACT_APP_API_POST_REDEEM
  return {
    type: REDEEM.GET_REDEEM,
    payload: fetch(url, {
      method: 'post',
      headers: {
        'Authorization': 'Bearer ' + data.jwtToken
      },
      body:JSON.stringify({code:data.code})
    }).then(response => response.json()).then(
      result => {
          return result;
      },
      error => console.log('Error API : ', error)
    )
  };
};



export const goHistory = jwtToken => {
  const url =
    process.env.REACT_APP_API_SERVER_HOST +
    process.env.REACT_APP_API_POST_HISTORY;
  return {
    type: HISTORY.GET_HISTORY,
    payload: fetch(url, {
      method: 'post',
      headers: {
        Authorization: 'Bearer ' + jwtToken
      }
    })
      .then(response => response.json())
      .then(
        result => {
          return result;
        },
        error => console.log('Error API : ', error)
      )
  };
};
