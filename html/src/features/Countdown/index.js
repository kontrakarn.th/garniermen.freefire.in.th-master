import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

export default props => {
  const [seconds, setSeconds] = useState('00');
  const [minutes, setMinutes] = useState('00');
  const [hours, setHours] = useState('00');
  const [days, setDays] = useState('0');


  const getTimeUntil = deadline => {
    let arr = deadline.split(/[- :]/),
    date_dead = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);

    const time = date_dead - Date.parse(new Date());
    // console.log("time",time)
    if (time < 0) {
      const seconds = '00';
      const minutes = '00';
      const hours = '00';
      const days = '0';

      setSeconds(seconds);
      setMinutes(minutes);
      setHours(hours);
      setDays(days);
    } else {
      const seconds = ('0' + Math.floor((time / 1000) % 60)).slice(-2);
      const minutes = ('0' + Math.floor((time / 1000 / 60) % 60)).slice(-2);
      const hours = ('0' + Math.floor((time / (1000 * 60 * 60)) % 24)).slice(
        -2
      );
      const days = ('0' + Math.floor(time / (1000 * 60 * 60 * 24))).slice(-3);

      setSeconds(seconds);
      setMinutes(minutes);
      setHours(hours);
      setDays(days);
    }
  };

  useEffect(() => {
    setInterval(() => getTimeUntil(props.deadline), 1000);
  });
  return (
    <CountdownTimer key={props.name}>
       {`เหลือระยะเวลากิจกรรม: ${days}D : ${hours}H : ${minutes}M`}
    </CountdownTimer>
  );
};

const CountdownTimer =  styled.div`
    text-align: center;
`
