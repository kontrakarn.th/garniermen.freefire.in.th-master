import React,{ useEffect } from 'react';
import { connect } from 'react-redux';
import { imageList } from '../../constants/Import_Images';
import { setValues,goRedeem,goHistory } from '../../store/redux';
import styled,{ keyframes ,css } from 'styled-components';
import CountDown from '../Countdown';
const VendingMachine = (props) =>{

	const redeemCode = ()=>{
		if(props.play_animation==false){
			if(props.redeem_code=="" || props.redeem_code.length==0){
				props.setValues({modal_open:"message",modal_message:"โปรดกรอกรหัส"})
			}else{
				props.goRedeem({jwtToken:props.jwtToken,code:props.redeem_code})
			}
		}
	}

	const openHistory = ()=>{
		if(props.history && props.history.length === 0){
			props.goHistory(props.jwtToken)
		}else{
			props.setValues({modal_open: 'history'})
		}
	}

	useEffect(() => {
		// Update the document title using the browser API
		if(props.play_animation==true){
			setTimeout(()=>{
				props.setValues({play_animation:false,modal_open:'reward',redeem_code:""})
			},1500)
		}
	});

	return(
		<ContentWrapper>
				<CountDownBox><CountDown deadline={props.end_at}/></CountDownBox>
				<ContentBox>
						<img className="frame" src={imageList['frame_inner']} alt=""/>
						<img className="ring" src={imageList['bg_eff']} alt=""/>
						<img className="logo" src={imageList['bg_dragon']} alt=""/>
						<div className="content">
								<img className="content__logo" src={imageList['logo']} alt=""/>
								<div className="content__name">
										{props.account_id} : {props.account_name}
								</div>
								<img className="content__itemcodeimg" src={imageList['text_itemcode']} alt=""/>
								<div className="content__inputwrapper">
										<input className="content__inputwrapper--input" type="text" value={props.redeem_code} onChange={(e)=>props.setValues({redeem_code: e.target.value})}/>
								</div>
								<img className={"content__btn" + (props.play_animation==true ? " disabled" : "")} src={imageList['btn_confirm']} alt="" onClick={()=>redeemCode()}/>
						</div>
				</ContentBox>
				<AnimationBox play={props.play_animation}>
						<div className="door"/>
						<img className="itemdrop" src={imageList['token']} alt=""/>
				</AnimationBox>
				<Btnbox>
						<Btns src={imageList['btn_history']} alt='' onClick={()=>openHistory()}/>
						<Btns className="h2p" src={imageList['btn_h2p']} alt='' onClick={()=>props.setValues({modal_open: 'h2p'})}/>
						<Btns onClick={()=>props.setValues({authState: 'LOGGED_OUT'})} src={imageList['btn_logout']} alt=''/>
				</Btnbox>
		</ContentWrapper>
	)
}

const mapStateToProps = state => ({ ...state.Main });

const mapDispatchToProps = { setValues,goRedeem,goHistory };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VendingMachine);

const RotateEff = keyframes`
     0%    {
          -webkit-transform: translate3d(-50%,-50%,0) rotate(0);
          -ms-transform: translate3d(-50%,-50%,0) rotate(0);
          transform: translate3d(-50%,-50%,0) rotate(0);
      }
    100%  {
        -webkit-transform: translate3d(-50%,-50%,0) rotate(360deg);
        -ms-transform: translate3d(-50%,-50%,0) rotate(360deg);
        transform: translate3d(-50%,-50%,0) rotate(360deg);
    }
`;
const DropEff = keyframes`
	0%{
		-webkit-transform: translate(0,-50%);
        -ms-transform: translate(0,-50%);
		transform: translate(0,-50%);
		opacity: 0;
	}
	98%{
		-webkit-transform: translate(0,0);
        -ms-transform: translate(0,0);
		transform: translate(0,0);
		opacity: 1;
	}
	100%{
		-webkit-transform: translate(0,0);
        -ms-transform: translate(0,0);
		transform: translate(0,0);
		opacity: 1;
	}
`

const ContentWrapper = styled.div`
   width: 100%;
   height: 100%;
   background: top center no-repeat url(${imageList['bg_machine']});
   background-size: 100% 100%;
   display: flex;
   justify-content: space-between;
   align-items: flex-end;
   position: relative;
`
const ContentBox = styled.div`
	width: 67%;
    height: 64%;
    position: absolute;
    top: 11%;
    left: 4%;
	overflow: hidden;
	background: #0d1419;
	.frame{
		position: absolute;
	    display: block;
	    top: -3%;
	    right: -2%;
	    margin: 0 auto;
	    max-width: none;
	    max-height: none;
	    width: 104%;
	    height: 102%;
	    pointer-events: none;
	}
	.ring{
		position: absolute;
	    display: block;
	    top: 50%;
	    left: 50%;
	    margin: 0 auto;
	    max-width: none;
	    max-height: none;
	    width: 80vh;
	    height: 80vh;
	    pointer-events: none;
	    transform: translate3d(-50%,-50%,0);
	    animation: ${RotateEff} 15s linear infinite;
	}
	.logo{
		position: absolute;
	    display: block;
	    top: 1%;
	    right: 10%;
	    margin: 0 auto;
	    max-width: none;
	    max-height: none;
	    width: 81%;
	    height: 93%;
	    pointer-events: none;
	}
	.content{
		width: 100%;
		height: 100%;
		box-sizing: border-box;
		padding: 6%;
		text-align: center;
		&__logo{
			display: block;
			margin: 0 auto;
			max-width: 60%;
		}
		&__name{
			text-align: center;
			font-size: 6vh;
			white-space: nowrap;
			line-height: 1;
			height: 17%;
			display: flex;
			justify-content: center;
			align-items: center;
		}
		&__itemcodeimg{
			display: block;
			margin: -8% auto 0;
			max-width: 60%;
		}
		&__inputwrapper{
			background: bottom center no-repeat url(${imageList['bg_inputbox']});
			background-size: 100% 115%;
		    width: 90%;
		    height: 28%;
		    margin: 0 auto;
			&--input{
			    height: 55%;
			    width: 85%;
			    background: linear-gradient(to bottom,#ffffff,#f5fff5);
			    color: #000000;
			    font-size: 5vh;
			    text-align: center;
			    box-sizing: border-box;
			    padding: 1% 4%;
			    position: relative;
			    top: 25%;
			}
		}
		&__btn{
			cursor: pointer;
			display: block;
			margin: 2% auto 0;
			max-width: 65%;
			&.disabled{
				filter: grayscale(1);
				pointer-events: none;
			}
		}
	}
`

const Btnbox = styled.div`
	width: 23%;
    height: 46.5%;
    position: absolute;
    bottom: 10%;
    right: 1.9%;
`
const Btns = styled.img`
	display: block;
	margin: 0 auto;
	cursor: pointer;
	&.h2p{
		margin: 3% auto;
	}
`
const AnimationBox = styled.div`
	position: absolute;
    width: 55%;
    height: 14%;
    margin: 0 auto;
    bottom: 8%;
    left: 10.3%;
    box-sizing: border-box;
    padding: 1%;
    padding: 0 1% 1%;
    overflow: hidden;
    .door{
    	background: #000000;
    	width: 100%;
    	transition: height 0.4s ease-out;
    	border-bottom: 2px solid #b6b6b6;
    	border-radius: 5px;
    	min-height: 15%;
    	height: ${props=>props.play ? 15 : 100}%;
    	position: relative;
    	z-index: 10;
    }
    .itemdrop{
		position: absolute;
	    max-height: none;
	    top: -56%;
	    left: 25%;
	    width: 50%;
	    height: 200%;
	    z-index: 5;
	    opacity: 0;
	     ${props=>props.play && css`
	     		animation: ${DropEff} 0.4s 0.4s ease-out forwards;
	     `}
    }
`

const CountDownBox = styled.div`
	display: block;
	position: absolute;
	top: 1%;
    left: 8%;
    background: center no-repeat url(${imageList['bg_countdown']});
    background-size: 103% 140%;
    width: 58%;
    height: 8%;
    display: flex;
    justify-content: center;
    align-items: center;
    // font-size: 4.5vh;
    font-size: 3.5vmin;
    white-space: nowrap;
    pointer-events: none;
    @media all and (device-width: 1024px) and (device-height: 768px) and (orientation:landscape){
			font-size: 3vmin;
	}
	@media all and (device-width: 1366px) and (device-height: 1024px) and (orientation:landscape){
			font-size: 3vh;
	}
`
