import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import { imageList } from '../../constants/Import_Images';

const CPN = props => {
  return (
    <ModalCore
      modal_name="warning"
      allowOutsideClicked={true}
      close_btn={true}
    >
      <ModalContent>
            
      </ModalContent>
    </ModalCore>
  );
};

const mapStateToProps = state => ({ ...state.Main });
const mapDispatchToProps = { setValues };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CPN);

const ModalContent = styled.div`
  position: relative;
  width: calc(100vw / 1920 * 1382);
  height: calc(100vh / 1080 * 733);
  background: top center no-repeat url(${imageList['modal_warning']});
  background-size: 100% 100%;
  color: #ffffff;
  word-break: break-word;
  padding: 4%;
  box-sizing: border-box;
  .text{
    &--green{
      color:#1b361e;
    }
    &--blue{
      color:#0b2e47;
    }
    &--purple{
      color:#460b3c;
    }
    &--gold{
      color:#bc4c00;
    }
  }
  .inner_title {
    width: 100%;
    height: 20%;
    > img {
      display: block;
      margin: 0 auto;
    }
  }
  .content {
    width: 100%;
    height: 60%;
    padding: 1% 0;
    margin: 3% auto 0;
    text-align: center;
    color: #000000;
    &.h2p {
      text-align: left;
      color: #ffffff;
      @media (max-width: 767px) {
        font-size: 1.2em;
      }
      h4 {
        margin: 0 0 1em 0;
        text-align: center;
      }
      ul {
        width: 96%;
        height: 100%;
        overflow: auto;
        margin: 0;
        padding-left: 4%;
        border-radius: 5px;
        padding-right: 3%;
        font-size: 1.8em;
        list-style-type: none;
        div {
          text-align: left;
          font-size: 1.1em;
          font-weight: bold;
          margin: 1% auto;
        }
        &::-webkit-scrollbar {
          width: 15px;
          background: #070707;
          @media(max-width: 1024px){
            width: 10px;
          }
        }
        &::-webkit-scrollbar-thumb {
          background: #00ac4f;
        }
        > li {
          width: 100%;
          text-align: left;
          line-height: 1.3;
        }
      }
    }
  }
`;
const TitleBox = styled.div`
  display: block;
  margin: 0 auto;
  font-size: 4em;
  color: #ffffff;
  text-align: center;
  height: 15%;
  line-height: 1;
  font-family: 'PSL-EmpireExtra';
  >img{
    display: block;
    margin: 0 auto;
  }
`;
const CheckBox = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 8%;
    margin: 1% auto;
    white-space: nowrap;
    font-size: 1.2em;
    .check{
        background: #ffffff;
        border: 2px solid #ffffff;
        position: relative;
        width: 40px;
        height: 40px;
        margin: 0 1%;
        cursor: pointer;
        @media(max-width: 1024px){
            width: 15px;
            height: 15px;
        }
        ${props=>props.policystatus &&
            `
                &:after{
                    content: '✓';
                    position: absolute;
                    width: 80%;
                    height: 80%;
                    top: 0;
                    left: 0;
                    bottom: 0;
                    right: 0;
                    margin: auto;
                    color: #000000;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    font-size: 3.5vh;
                }
            `
        }
    }
`

const Btns = styled.img`
    display: block;
    margin: 2% auto 0;
    max-width: 40%;
`
