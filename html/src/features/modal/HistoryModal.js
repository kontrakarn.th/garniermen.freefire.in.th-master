import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import { imageList } from '../../constants/Import_Images';

const HistoryModal = props => {
  return (
    <ModalCore
      modal_name="history"
      allowOutsideClicked={() => props.setValues({ modal_open: '' })}
      close_btn={true}
    >
      <ModalContent>
        <TitleBox>ประวัติรางวัล</TitleBox>
        <div className="content h2p">
          <ul>
          {
              // props.history && props.history
              props.history && props.history.length>0 && props.history.map((item, key) => {
                return(
                      <div className="historylist" key={`historylist_${key+1}`}>
                              <div className="itembox">
                                    {/*<img src={imageList['item_history']} alt=""/>*/}
                                    {item.code}
                              </div>
                              <div className="info">
                                    <div className="info__name">
                                          {/*item.item_name*/}
                                    </div>
                                    <div className="info__date">
                                          {item.created_at}
                                    </div>
                              </div>
                      </div>
                  )
              })
          }
          </ul>
        </div>
      </ModalContent>
    </ModalCore>
  );
};

const mapStateToProps = state => ({ ...state.Main });
const mapDispatchToProps = { setValues };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HistoryModal);

const ModalContent = styled.div`
  position: relative;
  width: calc(100vw / 1920 * 1387);
  height: calc(100vh / 1080 * 733);
  background: top center no-repeat url(${imageList['bg_modal']});
  background-size: 100% 100%;
  color: #ffffff;
  word-break: break-word;
  padding: 4%;
  box-sizing: border-box;
  .text{
    &--green{
      color:#1b361e;
    }
    &--blue{
      color:#0b2e47;
    }
    &--purple{
      color:#460b3c;
    }
    &--gold{
      color:#bc4c00;
    }
  }
  .inner_title {
    width: 100%;
    height: 20%;
    > img {
      display: block;
      margin: 0 auto;
    }
  }
  .content {
    width: 95%;
    height: 76%;
    padding: 0% 0;
    margin: 1% auto 0;
    text-align: center;
    color: #000000;
    .historylist{
      display: flex;
      justify-contetn: space-between;
      align-items: center;
      margin: 1% auto;
      /* height: 30%; */
      width: 80%;
      margin: 2% auto 0;
      .itembox{
          width: 32%;
          height: 100%
          border-right: 0.3vh solid #00ac4f;
          >img{
            display: block;
            margin: 0 auto;
          }
      }
    }
    &::-webkit-scrollbar {
      width: 8px;
      background: #002348;
    }
    &::-webkit-scrollbar-thumb {
      background: #ff00d8;
    }
    &.h2p {
      text-align: left;
      color: #ffffff;
      @media (max-width: 767px) {
        font-size: 1.2em;
      }
      h4 {
        margin: 0 0 1em 0;
        text-align: center;
      }
      ul {
        width: 96%;
        height: 100%;
        overflow: auto;
        margin: 0;
        padding-left: 4%;
        border-radius: 5px;
        padding-right: 3%;
        font-size: 1.5em;
        .info{
          text-align: left;
          font-size: 1.1em;
          line-height: 1;
          width: 60%;
          margin-left: 5%;
          &__name{
            color: #00ac4f;
          }
        }
        &::-webkit-scrollbar {
          width: 15px;
          background: #070707;
          @media(max-width: 1024px){
            width: 10px;
          }
        }
        &::-webkit-scrollbar-thumb {
          background: #00ac4f;
        }
        > li {
          width: 100%;
          text-align: left;
          line-height: 1.3;
        }
      }
    }
  }
`;
const TitleBox = styled.div`
  display: block;
  margin: 0 auto;
  font-size: 4em;
  color: #ffffff;
  text-align: center;
  height: 15%;
  line-height: 1;
  font-family: 'PSL-EmpireExtra';
  >img{
    display: block;
    margin: 0 auto;

  }
`;
