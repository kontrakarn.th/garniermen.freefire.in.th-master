import React from 'react';
import { connect } from 'react-redux';
import styled,{keyframes} from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import { imageList } from '../../constants/Import_Images';

const CPN = props => {

  return (
    <ModalCore modal_name="reward" allowOutsideClicked={true} close_btn={true} >
      <ModalMessageContent>
        <div className="content">
                  <TitleBox><img src={imageList['title_reward']} alt=""/></TitleBox>
                  <AnimationBox>
                      <img className="ring" src={imageList['eff_ring']} alt=""/>
                      <img className="logo" src={imageList['eff_dragon']} alt=""/>
                      <img className="item" src={imageList['fs_token']} alt=""/>
                      <img className="itemname" src={imageList['item_name']} alt=""/>
                  </AnimationBox>
                  <Btn onClick={()=>props.setValues({modal_open:''})} src={imageList['btn_confirm']} alt='confirm_btn' />
        </div>
      </ModalMessageContent>
    </ModalCore>
  );
};

const mapStateToProps = state => ({ ...state.Main });
const mapDispatchToProps = { setValues };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CPN);


const RotateEff = keyframes`
     0%    {
          -webkit-transform: translate3d(-50%,-50%,0) rotate(0);
          -ms-transform: translate3d(-50%,-50%,0) rotate(0);
          transform: translate3d(-50%,-50%,0) rotate(0);
      }
    100%  {
        -webkit-transform: translate3d(-50%,-50%,0) rotate(360deg);
        -ms-transform: translate3d(-50%,-50%,0) rotate(360deg);
        transform: translate3d(-50%,-50%,0) rotate(360deg);
    }
`;

const ModalMessageContent = styled.div`
    position: relative;
    width: calc(100vw / 1920 * 1289);
    height: calc(100vh / 1080 * 881);
    color: #FFFFFF;
    box-sizing: border-box;
    border-radius: 5px;
    word-break: break-word;
    display: flex;
    justify-content: center;
    align-items: center;
    .content{
        width: 100%;
        height: 100%;
        display: block;
        text-align: center;
    }
`;



const TitleBox = styled.div`
  display: block;
  margin: 0 auto;
  font-size: 4em;
  color: #ffffff;
  text-align: center;
  height: 23%;
  line-height: 1;
  position: relative;
  z-index: 20;
  >img{
    display: block;
    margin: 0 auto;
    max-width: 55%;
  }
`
const AnimationBox = styled.div`
    width: 50%;
    height: 63%;
    margin: 0 auto;
    .ring{
      position: absolute;
        display: block;
        top: 50%;
        left: 50%;
        margin: 0 auto;
        max-width: none;
        max-height: none;
        width: 55vh;
        height: 55vh;
        pointer-events: none;
        transform: translate3d(-50%,-50%,0);
        animation: ${RotateEff} 15s linear infinite;
    }
    .logo{
      position: absolute;
        display: block;
        top: 49%;
        left: 50%;
        transform: translate3d(-50%,-50%,0);
        margin: 0 auto;
        max-width: none;
        max-height: none;
        width: 32%;
        height: 47%;
        pointer-events: none;
    }
    .item{
      position: relative;
      z-index: 50;
      max-width: 96%;
      max-height: 77%;
    }
    .itemname{
      position: absolute;
      bottom: 15%;
      left: 0;
      right: 0;
      z-index: 50;
      margin: 0 auto;
      max-width: 72%;
    }
`
const NameBox = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 50%;
    height: 10%;
    font-size: 12vh;
    margin: 0 auto;
    color: #28f5d5;
    text-shadow: 4px 4px 4px #9b1a88, 6px 5px 10px #9b1a88, 4px 4px 10px #9B1A88;
    >img{
      display: block;
      margin: 0 2%;
    }
`

const Btn = styled.img`
  display: block;
  margin: 0 auto;
  cursor: pointer;
  max-width: 37%;
  max-height: 20%;
`
