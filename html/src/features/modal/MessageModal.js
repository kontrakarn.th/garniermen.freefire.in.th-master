import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import { imageList } from '../../constants/Import_Images';

const CPN = props => {
  let { modal_message } = props;
  return (
    <ModalCore modal_name="message" allowOutsideClicked={true} close_btn={true}>
      <ModalMessageContent>
        <div className="content">
          <div
            className="text"
            dangerouslySetInnerHTML={{ __html: modal_message }}
          />
        </div>
      </ModalMessageContent>
    </ModalCore>
  );
};

const mapStateToProps = state => ({ ...state.Main });
const mapDispatchToProps = { setValues };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CPN);

const ModalMessageContent = styled.div`
  position: relative;
  width: calc(100vw / 1920 * 1333);
  height: calc(100vh / 1080 * 931);
  color: #ffffff;
  background: top center no-repeat url(${imageList['bg_map']});
  background-size: 100% 100%;
  word-break: break-word;
  padding: 7% 5% 7%;
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  align-items: center;
  .content {
    display: block;
    text-align: center;
    .text {
      font-size: 3em;
      color: #ffffff;
      text-shadow: 0 0 0;
    }
  }
`;