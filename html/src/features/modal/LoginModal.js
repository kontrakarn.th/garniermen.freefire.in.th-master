import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import { imageList } from '../../constants/Import_Images';

const CPN = props => {
  return (
    <ModalCore modal_name="login" allowOutsideClicked={true} close_btn={true}>
      <ModalMessageContent>
        <TitleBox><img src={imageList['text_title_login']} alt=""/></TitleBox>
        <div className="content">
          <ModalBottom>
            <Btns>
              <a href={props.loginUrl && props.loginUrl.replace('{{platform}}', 3)}>
                <img src={imageList['btn_fb']} alt="" onClick={() => props.setValues({ modal_open: '' })}/>
              </a>
            </Btns>
            <Btns>
              <a href={props.loginUrl && props.loginUrl.replace('{{platform}}', 5)}>
                <img
                  src={imageList['btn_vk']}
                  alt=""
                  onClick={() => props.setValues({ modal_open: '' })}
                />
              </a>
            </Btns>
          </ModalBottom>
        </div>
      </ModalMessageContent>
    </ModalCore>
  );
};

const mapStateToProps = state => ({ ...state.Main });
const mapDispatchToProps = { setValues };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CPN);

const ModalMessageContent = styled.div`
  position: relative;
  width: calc(100vw / 1920 * 1395);
  height: calc(100vh / 1080 * 854);
  color: #ffffff;
  background: top center no-repeat url(${imageList['bg_map']});
  background-size: 100% 100%;
  word-break: break-word;
  padding: 7% 5% 7%;
  box-sizing: border-box;
  .content {
    // display: block;
    text-align: center;
    width: 80%;
    margin: 0 auto;
    height: 75%;
    display: flex;
    justify-content: center;
    align-items: center;
    .text {
      font-size: 3em;
      color: #ffffff;
      text-shadow: 0 0 0;
    }
  }
`;
const ModalBottom = styled.div`
  display: block;
  margin: 5% auto 0;
  position: relative;
  z-index: 50;
`;
const Btns = styled.div`
  margin: 2% auto;
  display: block;
  >a{
    display: block;
    margin: 0 auto;
     > img {
      display: block;
      cursor: pointer;
      margin: 0 auto;
      border-radius: 2px;
    }
  }
  &:hover {
    filter: brightness(1.1);
  }
`;

const TitleBox = styled.div`
  display: block;
  margin: 0 auto;
  font-size: 4em;
  color: #000000;
  text-align: center;
  height: 25%;
  >img{
    display: block;
    margin: 0 auto;
  }
`;

