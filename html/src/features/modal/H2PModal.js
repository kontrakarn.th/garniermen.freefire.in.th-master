import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import { imageList } from '../../constants/Import_Images';

const CPN = props => {
  return (
    <ModalCore
      modal_name="h2p"
      allowOutsideClicked={true}
      close_btn={true}
    >
      <ModalContent>
        <TitleBox>วิธีการรับไอเทม</TitleBox>
        <div className="content h2p">
        <ul>
            <li><strong>ระยะเวลากิจกรรม</strong></li>
            <li>ตั้งแต่ วันที่ 24 กุมภาพันธ์ 2563 เวลา 10.00 น. ถึงวันที่ 30 มิถุนายน 2563 เวลา 23.59 น. หรือจนกว่าของรางวัลจะหมด</li>
            <li><strong>เงื่อนไขและกติกาการเข้าร่วมรายการ</strong></li>
            <li>1. ผู้ร่วมรายการสามารถรับรหัสตัวเลขจากการซื้อผลิตภัณฑ์โฟมล้างหน้า การ์นิเย่ เมน แอคโนไฟท์ ที่มีฉลากลาย FREE FIRE โดยมีรายละเอียดดังนี้</li>
            <li className="out_dent">1.1 ชุดเซ็ตพิเศษผลิตภัณฑ์โฟมล้างหน้า การ์นิเย่ เมน แอคโนไฟท์ ขนาด 15 มล. 2 หลอด มูลค่า 40 บาท จะได้รับ 1 รหัส</li>
            <li className="out_dent">1.2 ผลิตภัณฑ์โฟมล้างหน้า การ์นิเย่ เมน แอคโนไฟท์ ขนาด 50 มล. มูลค่า 69 บาท จะได้รับ 1 รหัส</li>
            <li className="out_dent">1.3 ผลิตภัณฑ์โฟมล้างหน้า การ์นิเย่ เมน แอคโนไฟท์ ขนาด 100 มล. มูลค่า 119 บาท หรือขนาด 150 มล. มูลค่า149 บาท จะได้รับ 2 รหัส</li>
            <li>2. ส่งรหัสตัวเลขหลังฉลากผลิตภัณฑ์ที่ได้รับ (จำนวน 10 หลัก) เพื่อรับไอเทม GARNIER MEN x FREE FIRE BOX จำนวน 1 ชิ้น (มูลค่า 60 บาท) ต่อ 1 รหัส โดยผู้ร่วมรายการมีสิทธิ์ได้รับของรางวัลภายในกล่องซึ่งประกอบไปด้วย</li>
            <li className="out_dent">- ชุดแต่งกายถาวร Limited Edition “กังฟูดราก้อน” สำหรับตัวละครชาย</li>
            <li className="out_dent">- ไอเทมแอร์บอร์ด</li>
            <li className="out_dent">- ไอเทมร่มชูชีพ</li>
            <li className="out_dent">- ไอเทมแผนที่เสบียง</li>
            <li className="out_dent">- ไอเทมกองไฟ 3 วัน</li>
            <li className="out_dent">- โกลด์ จำนวน 300 หน่วย</li>
            <li>3. บริษัทฯ ขอสงวนสิทธิ์ให้ของรางวัลแก่ผู้ร่วมรายการที่เข้าสู่ระบบด้วยบัญชีของ Free Fire ที่ผูกไว้กับบัญชี Facebook, Google, หรือ VK เท่านั้น</li>
            <li>4. ผู้ร่วมรายการจะได้รับ GARNIER MEN x FREE FIRE BOX เข้ากล่องจดหมายในเกม FREE FIRE ภายหลังจากส่งรหัสหลังฉลากเรียบร้อยแล้ว</li>
            <li>5. ผู้ร่วมรายการสามารถส่งรหัสได้ไม่จำกัดจำนวนครั้งต่อวัน</li>
            <li>6. ในกรณีที่ผู้ร่วมรายการส่งรหัสซ้ำ หรือกรอกรายละเอียดไม่ถูกต้องเกินจำนวนครั้งที่กำหนด การีนาขอสงวนสิทธิ์ในการไม่อนุญาตให้ผู้ร่วมรายการส่งรหัสได้ ภายในระยะเวลาที่ทางการีนากำหนด</li>
            <li>7. มูลค่าของรางวัลเป็นมูลค่า ณ วันที่ 24 กุมภาพันธ์ 2563</li>
            <br/>
            <li><strong>เงื่อนไขอื่นๆ</strong></li>
            <li>1. ของรางวัลที่ได้รับไม่สามารถจำหน่ายหรือแลกเปลี่ยนเป็นเงินสดหรือของรางวัลอื่นได้ และไม่สามารถโอนสิทธิ์ในการได้รับของรางวัลให้กับบุคคลอื่นได้ไม่ว่ากรณีใด</li>
            <li>2. ของรางวัลมีจำนวนจำกัด</li>
            <li>3. การีนาขอสงวนสิทธิ์ในการเปลี่ยนแปลงรายละเอียดของกิจกรรมโดยไม่ต้องแจ้งให้ทราบล่วงหน้า</li>
            <li>4. คำตัดสินของการีนาถือเป็นที่สิ้นสุด</li>
            <li>5. การีนาขอสงวนสิทธิ์ไม่รับผิดชอบต่อความผิดพลาดอันเกิดขึ้นจากระบบอินเทอร์เน็ตที่ล่าช้า ไม่สมบูรณ์ หรือเกิดจากการฉ้อโกงหรือการส่งข้อมูลที่ไม่ถูกต้อง ที่เกิดจากผู้เข้าร่วมรายการ ไม่ว่าจะเนื่องมาจากสาเหตุของความผิดพลาด การเพิกเฉย การแก้ไขปรับเปลี่ยน การให้สินบน การลบทิ้ง ขโมย การทำลายข้อมูลโดยไม่ได้รับอนุญาตหรือการลักลอบใช้ข้อมูล ความเสียหายของข้อมูล เครือข่ายล้มเหลว หรือความผิดพลาดของซอฟต์แวร์หรือฮาร์ดแวร์ หรือไม่ว่าเหตุใด</li>
            <li>6. การีนาขอสงวนสิทธิ์ในการนำภาพถ่ายและ/หรือเสียงของผู้ร่วมรายการ โดยถือเป็นสิทธิ์ขาดของการีนาที่จะสามารถนำไปเผยแพร่ และประชาสัมพันธ์ได้ โดยไม่ต้องขออนุญาตหรือจ่ายค่าตอบแทน</li>
        </ul>
        </div>
      </ModalContent>
    </ModalCore>
  );
};

const mapStateToProps = state => ({ ...state.Main });
const mapDispatchToProps = { setValues };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CPN);

const ModalContent = styled.div`
  position: relative;
  width: calc(100vw / 1920 * 1382);
  height: calc(100vh / 1080 * 733);
  background: top center no-repeat url(${imageList['bg_modal']});
  background-size: 100% 100%;
  color: #ffffff;
  word-break: break-word;
  padding: 4%;
  box-sizing: border-box;
  font-size: 0.6em;
  .text{
    &--green{
      color:#1b361e;
    }
    &--blue{
      color:#0b2e47;
    }
    &--purple{
      color:#460b3c;
    }
    &--gold{
      color:#bc4c00;
    }
  }
  .inner_title {
    width: 100%;
    height: 20%;
    > img {
      display: block;
      margin: 0 auto;
    }
  }
  .content {
    width: 100%;
    height: 75%;
    padding: 1% 0;
    margin: 1% auto 0;
    text-align: center;
    color: #000000;
    &.h2p {
      text-align: left;
      color: #ffffff;
      @media (max-width: 767px) {
        font-size: 1.2em;
      }
      h4 {
        margin: 0 0 1em 0;
        text-align: center;
      }
      ul {
        width: 96%;
        height: 100%;
        overflow: auto;
        margin: 0;
        padding-left: 4%;
        border-radius: 5px;
        padding-right: 3%;
        font-size: 1.8em;
        list-style-type: none;
        div {
          text-align: left;
          font-size: 1.1em;
          font-weight: bold;
          margin: 1% auto;
        }
        &::-webkit-scrollbar {
          width: 15px;
          background: #070707;
          @media(max-width: 1024px){
            width: 10px;
          }
        }
        &::-webkit-scrollbar-thumb {
          background: #00ac4f;
        }
        > li {
          width: 100%;
          text-align: left;
          line-height: 1.3;
          &.out_dent{
              padding-left:1em;
          }
        }
      }
    }
  }
`;
const TitleBox = styled.div`
  display: block;
  margin: 0 auto;
  font-size: 4em;
  color: #ffffff;
  text-align: center;
  height: 15%;
  line-height: 1;
  font-family: 'PSL-EmpireExtra';
  >img{
    display: block;
    margin: 0 auto;
  }
`;
