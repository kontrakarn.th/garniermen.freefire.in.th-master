import MessageModal from './MessageModal';
import LoadingModal from './LoadingModal';
import H2PModal from './H2PModal';
import RewardModal from './RewardModal';
import HistoryModal from './HistoryModal';
import PolicyModal from './PolicyModal';
import WarningModal from './WarningModal';

export {
  MessageModal,
  LoadingModal,
  H2PModal,
  RewardModal,
  HistoryModal,
  PolicyModal,
  WarningModal
};
