import React from 'react';
import ModalCore from './ModalCore.js';
import { connect } from 'react-redux';
import styled, { keyframes } from 'styled-components';

const LoadingModal = props => {
  return (
    <ModalCore modal_name="loading" allowOutsideClicked={false} close_btn={'false'}>
      <Loadingwrap color={'#5d7f0a'} shadow={'#161616'}>
        <div className="loading">
          <div className="loadingring" />
        </div>
      </Loadingwrap>
    </ModalCore>
  );
};

const mapStateToProps = state => ({
  ...state.Main
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoadingModal);

const LoadingAnimation = keyframes`
	 0% {
	    transform: rotate(0deg);
	  }
	  100% {
	    transform: rotate(360deg);
	  }
`;
const Loadingwrap = styled.div`
  .loading {
    text-align: center;
    height: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 0 auto;
  }
  .loadingring {
    display: flex;
    justify-content: center;
    align-items: center;
    @media (min-width: 1024px) {
      transform: scale(0.5);
    }
  }
  .loadingring:after {
    content: ' ';
    display: block;
    width: 10vw;
    height: 10vw;
    margin: 1px;
    border-radius: 50%;
    animation: ${LoadingAnimation} 1.2s linear infinite;
    ${props =>`
      filter: drop-shadow(0 0 1px ${props.shadow}) drop-shadow(0 0 2px ${props.shadow});
    `}
    ${props =>
      props.color &&
      `
				border: 0.7vw solid ${props.color};
				border-color: ${props.color} transparent ${props.color} transparent;
			`}
  }
`;
