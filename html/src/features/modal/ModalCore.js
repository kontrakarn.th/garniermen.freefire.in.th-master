import React from 'react';
import { connect } from 'react-redux';
import styled, { keyframes } from 'styled-components';
import { setValues } from '../../store/redux';
import { imageList } from '../../constants/Import_Images';

const CPN = props => {
  let { modal_open, close_btn } = props;
  let checkCloseBtn =
    ['confirmreward', 'reward', 'confirm'].indexOf(props.modal_name) < 0;

  const closeModal = (fn)=>{
          props.setValues({ modal_open: '' })
  }

  return (
    <Modal className={modal_open === props.modal_name ? 'open' : 'close'}>
      <ModalBackdrop
        onClick={() => {
          if (props.allowOutsideClicked) closeModal();
        }}
      />
      <div className="modalContent">
        <div className="contentwrap">
          {close_btn === true &&
                    <CloseBtn
                      className={!checkCloseBtn ? 'fixed' : null}
                      onClick={() => closeModal()}
                    >
                      <img src={imageList['btn_close']} alt="" />
                    </CloseBtn>
          }
          {props.children && props.children}
        </div>
      </div>
    </Modal>
  );
};

const mapStateToProps = state => ({ ...state.Main });
const mapDispatchToProps = { setValues };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CPN);

const ShowAnimation = keyframes`
	  0% {
	    transform: scale(.7);
	  }
	  45% {
	    transform: scale(1.05);
	  }
	  80% {
	    transform: scale(.95);
	  }
	  100% {
	    transform: scale(1);
	  }
`;

const Modal = styled.div`
  position: fixed;
  top: 0px;
  left: 0px;
  display: block;
  width: 100vw;
  height: 100vh;
  overflow: hidden;
  z-index: 7000;
  opacity: 0;
  pointer-events: none;
  .modalContent {
    position: absolute;
    display: flex;
    justify-content: center;
    align-items: center;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    pointer-events: none;
    .contentwrap {
      position: relative;
      max-width: 90%;
      max-height: 90%;
      letter-spacing: 1px;
      font-size: 1.2em;
    }
  }
  &.close {
    opacity: 0;
    pointer-events: none;
  }
  &.open {
    opacity: 1;
    pointer-events: all;
    .modalContent {
      animation: ${ShowAnimation} 0.2s alternate forwards;
      .contentwrap {
        pointer-events: visible;
      }
    }
  }
`;
const ModalBackdrop = styled.div`
  position: relative;
  display: block;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.7);
`;
const CloseBtn = styled.div`
  position: absolute;
  width: 4%;
  top: -5%;
  right: -5%;
  z-index: 50;
  > img {
    display: block;
    margin: 0 auto;
    cursor: pointer;
  }
`;
