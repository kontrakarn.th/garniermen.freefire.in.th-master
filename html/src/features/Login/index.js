import React from 'react';
import { imageList } from '../../constants/Import_Images';
import styled from 'styled-components';
import {connect} from 'react-redux';
import { setValues } from '../../store/redux';
const LoginPage = props => {
	return(
		<LoginWrapper>
				<Logo src={imageList['logo']} alt=''/>
				<ContentWrapper>
						<img className="content__left" src={imageList['content_item_left']} alt=""/>
						<img className="content__center" src={imageList['logo_event']} alt=""/>
						<img className="content__right" src={imageList['content_item_right']} alt=""/>
				</ContentWrapper>
				<LoginContent>
						<div className="title">เข้าสู่ระบบ/ลงทะเบียน</div>
						<BtnBox>
								<a href={props.loginUrl && props.loginUrl.replace('{{platform}}', 3)} className="loginbtns"><img src={imageList['btn_fb']} alt=""/></a>
								<a href={props.loginUrl && props.loginUrl.replace('{{platform}}', 8)} className="loginbtns"><img src={imageList['btn_gg']} alt=""/></a>
								<a href={props.loginUrl && props.loginUrl.replace('{{platform}}', 5)} className="loginbtns"><img src={imageList['btn_vk']} alt=""/></a>
						</BtnBox>
				</LoginContent>
		</LoginWrapper>
	)
}

const mapStateToProps = state => ({ ...state.Main });

const mapDispatchToProps = { setValues };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);

const LoginWrapper = styled.div`
	display: block;
	margin: 0 auto;
	width: 100%;
	height: 100%;
	position: relative;
	box-sizing: border-box;
`
const Logo = styled.img`
	display: block;
    position: absolute;
    top: 3%;
    left: 2%;
    max-width: 31%;
`
const ContentWrapper = styled.div`
	display: flex;
	justify-content: space-around;
	align-items: flex-start;
	width: 100%;
	height: 75%;
	margin: 0 auto;
	.content{
		&__left{
			position: absolute;
		    left: 0;
		    top: 0;
		    pointer-events: none;
		    max-width: none;
		    max-height: none;
		    width: 43.5%;
		    height: 89%;
		    order: 2;
		}
		&__center{
			display: block;
		    margin: 7% auto;
		    max-width: 34%;
		    max-height: 30%;
		    order: 1;
		}
		&__right{
			position: absolute;
		    right: 0;
		    top: 5%;
		    pointer-events: none;
		    max-width: none;
		    max-height: none;
		    width: 38.5%;
		    height: 76%;
		    order: 3;
		}
	}
`
const LoginContent = styled.div`
	display: block;
	margin: 0 auto;
    width: 100%;
	position: absolute;
    top: 39%;
    left: 0;
    height: 54%;
	.title{
		color:#d7f207;
		font-size: 3.7em;
	    text-align: center;
	    margin: 0 auto;
	    line-height: 1;
	}
`

const BtnBox = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	// width: 56%;
	width: 27%;
	margin: 3% auto 0;
    height: 75%;
    flex-flow: column;
	.loginbtns{
		cursor: pointer;
		width: 100%;
		>img{
			display: block;
			margin: 0 auto;
		}
	}
`
