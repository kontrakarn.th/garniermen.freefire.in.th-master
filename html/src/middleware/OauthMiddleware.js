import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getParam, setCookie, getCookie, delCookie } from './../utils/Tools';
import { getJwtToken, setLoginUrl, setLogoutUrl, setAuthState, getHomeInfo,setValues } from '../store/redux';
import { Loader } from '../common/Loader';

import styled from 'styled-components';

const OauthFail = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  background-color: rgba(0, 0, 0, 0.9);
  text-align: center;
  line-height: 100vh;
  z-index: 999999;
  h4 {
    color: red;
    margin-top: 40px;
  }
`;

class OauthMiddleware extends Component {
  componentWillMount() {
    this.checkLogin();
  }

  componentDidMount() {
      const loginUrl = `${
        process.env.REACT_APP_OAUTH_URL
      }/oauth/login?response_type=token&client_id=${
        process.env.REACT_APP_OAUTH_APP_ID
      }&redirect_uri=https://auth.garena.in.th/login/callback/${
        process.env.REACT_APP_OAUTH_APP_NAME
      }/&locale=${process.env.REACT_APP_LOCALE}&all_platforms=1&platform=${
        process.env.REACT_APP_OAUTH_PLATFORM
      }`;
    //
    this.props.setLoginUrl(loginUrl);
    // this.props.getHomeInfo(this.props.jwtToken)
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.authState !== this.props.authState) {
      switch (this.props.authState) {
        case 'LOGGED_IN':
          this.login();
          break;
        case 'LOGGED_OUT':
          this.logout();
          break;
        default:
      }

    }
    if ((this.props.jwtToken !== prevProps.jwtToken) && this.props.jwtToken!="") {
        this.props.getHomeInfo(this.props.jwtToken)
    }
    if((this.props.modal_type!=prevProps.modal_type) && this.props.modal_type=="cantplay"){
        this.props.setValues({jwtToken:""})
        setTimeout(()=>{
            this.logout()
        },2000)
    }
  }

  login() {
    setCookie(
      process.env.REACT_APP_OAUTH_COOKIE_NAME,
      this.props.sessionKey,
      30,
      process.env.REACT_APP_OAUTH_COOKIE_DOMAIN
    );
    let uri = window.location.href.substring(
      0,
      window.location.href.lastIndexOf('?')
    ); // get rid of the query string
    window.location.href = uri; // we need hard refresh
  }

  logout() {
      const oauthToken = getCookie(process.env.REACT_APP_OAUTH_COOKIE_NAME);
      delCookie(
        process.env.REACT_APP_OAUTH_COOKIE_NAME,
        // process.env.REACT_APP_OAUTH_COOKIE_DOMAIN
      );
      let redirect =
        'https://auth.garena.in.th/login/callback/' +
        process.env.REACT_APP_OAUTH_APP_NAME +
        '/logout';
      const logoutUrl = `${
        process.env.REACT_APP_OAUTH_URL
      }/oauth/logout?access_token=${oauthToken}&format=redirect&redirect_uri=${redirect}`;
      window.location.href = logoutUrl;
  }

  checkLogin() {
    var oauthParam = getParam(process.env.REACT_APP_OAUTH_PARAM_NAME) || getParam(process.env.REACT_APP_OAUTH_PARAM_NAME_2);
    var oauthCookie = getCookie(process.env.REACT_APP_OAUTH_COOKIE_NAME);

    if (oauthParam && oauthParam.length > 0) {
      setCookie(process.env.REACT_APP_OAUTH_COOKIE_NAME, oauthParam, 1);

      var url = window.location.href.substring(
        0,
        window.location.href.lastIndexOf('?')
      ); // get rid of the query string
      window.location.href = url; // we need hard refresh
    } else {
      if (oauthCookie) {
          if (oauthCookie.length>33) {
              this.props.getJwtToken({
                oauthCookie: oauthCookie
              });
          }else{
              delCookie(
                process.env.REACT_APP_OAUTH_COOKIE_NAME,
              );
              window.location.reload()
          }
      }
    }
  }

  render() {
    const { jwtError, jwtToken, jwtLoading } = this.props;
    if (jwtLoading) {
      return <Loader />;
    } else if (jwtError || jwtToken === undefined) {
        delCookie(
          process.env.REACT_APP_OAUTH_COOKIE_NAME,
        );
         window.location.reload()
      return (
        <OauthFail>
          <Loader />
          <h4>Please try again later _/\_</h4>
        </OauthFail>
      );

    } else {
      return null;
    }
  }
}

const mapStateToProps = state => ({ ...state.Main });

const mapDispatchToProps = {
  setAuthState,
  getJwtToken,
  setLoginUrl,
  setLogoutUrl,
  getHomeInfo,
  setValues,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OauthMiddleware);
