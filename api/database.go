package main

import (
	"os"
	"./common/database"
)

func initDbConfig() {
	databases := database.DatabaseConfigs{
		// add more config here
		database.DatabaseConfig{
			"cache",
			"redis",
			os.Getenv("REDIS_HOST"),
			os.Getenv("REDIS_PORT"),
			os.Getenv("REDIS_DB"),
			"",
			os.Getenv("REDIS_PASSWORD"),
		},
		// database.DatabaseConfig{
		// 	"mysql", // name
		// 	"mysql", // driver
		// 	os.Getenv("MYSQL_HOST"),
		// 	os.Getenv("MYSQL_PORT"),
		// 	os.Getenv("MYSQL_DATABASE"),
		// 	os.Getenv("MYSQL_USERNAME"),
		// 	os.Getenv("MYSQL_PASSWORD"),
		// },
		database.DatabaseConfig{
			"mongo",
			"mongo",
			os.Getenv("MONGO_HOST"),
			os.Getenv("MONGO_PORT"),
			"",
			os.Getenv("MONGO_USERNAME"),
			os.Getenv("MONGO_PASSWORD"),
		},
	}

	// do not edit
	database.SetDbConfig(databases)
}
