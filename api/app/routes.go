package app

import (
	_http "../common/http"
	"./Controller/Home"
	"./Controller/Redeem"
	// "./Controller/Exchange"
	// "./Controller/Redeem"
	"./Middleware"
)

func GetRoutes() _http.Routes {
	routes := _http.Routes{
		_http.Route{
			Name:        "getHomeInfo",
			Method:      "POST",
			Pattern:     "/api/gethomeinfo",
			HandlerFunc: Home.GetHomeInfo,
		},
		_http.Route{
			Name:        "Redeem",
			Method:      "POST",
			Pattern:     "/api/redeem",
			HandlerFunc: Redeem.Redeem,
		},
		// _http.Route{
		// 	Name:        "Exchange",
		// 	Method:      "POST",
		// 	Pattern:     "/api/exchange",
		// 	HandlerFunc: Exchange.Exchange,
		// },
		_http.Route{
			Name:        "getHistory",
			Method:      "POST",
			Pattern:     "/api/gethistory",
			HandlerFunc: Home.GetHistory,
		},
		_http.Route{
			Name:        "getOauth",
			Method:      "GET",
			Pattern:     "/api/oauth/user/info/get",
			HandlerFunc: Middleware.GetOauth,
		},

	}

	return routes
}
