package Middleware

import (
	// "encoding/json"
	// "io/ioutil"
	"log"
	"net/http"
	"os"

	// "fmt"
	// "io"
	// "strconv"
	// "sync"
	// "time"
	// "reflect"
	// "bytes"
	// "mime/multipart"
	// "strings"
	// "path/filepath"

	database "../../common/database"
	_http "../../common/http"
	Constant "../Constant"

	"github.com/dgrijalva/jwt-go"
	// common "../../common"
	// utils "../../common/utils"
	// "math/rand"
	// "time"
)

func GetOauth(w http.ResponseWriter, r *http.Request) {

	keys, ok := r.URL.Query()["access_token"]

	if !ok || len(keys[0]) < 1 {
		log.Print("param 'access_token' is missing")
		Constant.ResponseMessage(w, nil, "Error(1)", false)
		return
	}

	token := keys[0]
	result, err := database.CacheRemember(
		"Freefire:"+os.Getenv("EVENT_NAME")+":get_oauth:token:"+token,
		300, // 5 mins
		func() interface{} {
			r, err := _http.CurlGet(os.Getenv("OAUTH_HOST") + "/oauth/user/info/get?access_token=" + token)

			if err != nil {
				log.Print("error while getting oauth: ", err)
				return nil
			}
			return r
		},
	)
	if err == nil && result != nil {
		response := result.(map[string]interface{})
		if response["uid"] == nil {
			log.Print("error while getting oauth: invalid response")
		} else {
			// do jwt
			jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
				"uid":      response["uid"],
				"open_id":  response["open_id"],
				"gender":   response["gender"],
				"platform": response["platform"],
				"nickname": response["nickname"],
				"icon":     response["icon"],
			})

			// sign
			jwtTokenString, err := jwtToken.SignedString([]byte(os.Getenv("APP_SECRET")))
			if err != nil {
				log.Print("error while getting oauth: ", err)
				Constant.ResponseMessage(w, nil, "Error(2)", false)
				return
			}

			data := map[string]interface{}{
				"token": jwtTokenString,
			}

			Constant.ResponseMessage(w, data, "Success!", true)

			return
		}
	} else {
		log.Print("error while getting oauth: response is nil")
	}

	Constant.ResponseMessage(w, nil, "Error(3)", false)
}
