package Constant

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	_http "../../common/http"
)

type BasicResponse struct {
	Error   int         `json:"error"`
	Result  interface{} `json:"data"`
	Message string      `json:"message"`
	Status  bool        `json:"status"`
}

var Wording = map[string]string{
	"mongo_data_not_found": "mongo: no documents in result",
	"success":           "สำเร็จ",
	"sendsuccess":       "ทำรายการสำเร็จ",
	"keynotexist":       "ไม่พบรายการที่ต้องการ",
	"cantconnectdb":     "ไม่สามารถเชื่อมต่อข้อมูลได้",
	"default":           "เกิดข้อผิดพลาด",
	"fillrequire":       "โปรดกรอกข้อมูลให้ครบถ้วน",
	"noranking":         "คุณไม่ติดอันดับ ไม่สามารถกรอกข้อมูลได้",
	"endEvent":          "ยังไม่ถึงเวลา หรือ หมดกิจกรรม",
	"mustlogin":         "กรุณาล็อคอินก่อนร่วมกิจกรรม",
	"notfound":          "ไม่พบข้อมูล",
	"dailylimit":        "คุณไม่สามารถใช้สิทธิได้แล้ว กรุณารับใหม่ในวันถัดไป",
	"youcantdo":         "คุณไม่มีสิทธิในการทำรายการนี้",
	"cantplaygacha":     "เล่นกิจกรรมครบแล้วไม่สามารถเล่นเพิ่มได้",
	"nocharacter":       "ไม่พบตัวละคร",
	"nowallet":          "ไม่พบกระเป๋าเงิน",
	"nodata":            "ไม่พบข้อมูล",
	"noitemcart":        "ไม่พบไอเท็มที่ต้องการซื้อ",
	"processunsuccess":  "ทำรายการไม่สำเร็จ",
	"senditemunsuccess": "ส่งไอเท็มไม่สำเร็จ",
	"gemsnotenough":     "จำนวนไดมอนด์ไม่พอ",
	"timeoutbuy":        "หมดเวลาในการซื้อแล้ว",
	"notreadytobuy":     "ยังไม่สามารถซื้อได้",
	"buynotallow":       "ไม่สามารถซื้อไอเท็มที่เลือกได้",
	"buyonetimeonly":    "ซื้อได้ครั้งเดียวเท่านั้น",
	"gemscondition":     "ยังไม่ผ่านเงื่อนไข ไม่สามารถซื้อได้",
	"duplicateget":      "คุณรับไอเท็มไปแล้ว ไม่สามารถรับซ้ำได้",
	"duplicatebuy":      "ไม่สามารถซื้อซ้ำได้",
	"duplicateposition": "ไม่สามารถเลือกตำแหน่งซ้ำได้",
	"conditionnotpass":  "ยังไม่ผ่านเงื่อนไข โปรดลองใหม่หลังจากทำเงื่อนไขครบ",
	"compleateallquest":  "คุณผ่านเงื่อนไขครบแล้ว",
	"playerror":         "เกิดข้อผิดพลาดในการเล่น",
	"contactcs":         "เกิดข้อผิดพลาดในการทำรายการ โปรดติดต่อฝ่าย Support",
	"noGems":            "คุณมี Diamond ไม่เพียงพอ",
	"cantplay":          "This is a restricted event which can only be access in Thailand region only.",
	"timeout":           "ขณะนี้มีผู้ใช้เป็นจำนวนมาก โปรดลองใหม่อีกครั้งภายหลัง",
	"mustgetcurrentitem": "ไม่สามารถเล่นได้",
	"tokennotenough":    "จำนวนไม่พอ ไม่สามารถแลกได้",
	"alreadypass":		 "คุณได้ผ่านภารกิจนี้แล้ว",
	"buymorelimt":		 "ไม่สามารถซื้อไอเท็มเกินจำนวนที่กำหนด",
	"cantbuy":		     "ไม่สามารถซืื้อไอเท็มนี้ได้",
	"finishplay":		 "คุณเล่นครบแล้ว ไม่สามารถเล่นอีกได้",
	"ban_util":	     "คุณถูกแบน จะกรอกโค้ดได้อีก",
	"dup_or_wrong": "ท่านใส่รหัสไม่ถูกต้อง หรืือ รหัสถูกใช้งานแล้ว",

}

func ResponseMessage(w http.ResponseWriter, data interface{}, message string, status bool) {
	_http.JsonResponse(w, BasicResponse{
		Result:  data,
		Message: message,
		Status:  status,
	})
}

func FormJson(body io.Reader) map[string]interface{} {
	b, err := ioutil.ReadAll(body)
	if err != nil {
		log.Print("Error while reading request body (1): ", err)
		return nil
	}

	var req map[string]interface{}
	err2 := json.Unmarshal(b, &req)

	if err2 != nil {
		log.Print("Error while parsing request body (2): ", err2)
		// ResponseMessage(w, nil, "เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง", false)
		return nil
	}

	return req
}

func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

func makeTimestamp() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}

func anythingJsonToString(data interface{}) string {
	b, _ := json.Marshal(data)
	s := string(b)
	return s
}

func diffDatePerSecond(data string) float64 {
	now := time.Now()
	datetime := now.Format("2006-01-02 15:04:05")

	then, err := time.Parse("2006-01-02 15:04:05", data)
	if err != nil {
		log.Println(err)
		return 0
	}

	then1, err1 := time.Parse("2006-01-02 15:04:05", datetime)
	if err1 != nil {
		log.Println(err)
		return 0
	}

	diff := then1.Sub(then)

	return diff.Seconds()
}

func ipAddrFromRemoteAddr(s string) string {
	idx := strings.LastIndex(s, ":")
	if idx == -1 {
		return s
	}
	return s[:idx]
}

func GetIPAdress(r *http.Request) string {
	hdr := r.Header
	hdrRealIp := hdr.Get("X-Real-Ip")
	hdrForwardedFor := hdr.Get("X-Forwarded-For")
	if hdrRealIp == "" && hdrForwardedFor == "" {
		return ipAddrFromRemoteAddr(r.RemoteAddr)
	}
	if hdrForwardedFor != "" {
		// X-Forwarded-For is potentially a list of addresses separated with ","
		parts := strings.Split(hdrForwardedFor, ",")
		for i, p := range parts {
			parts[i] = strings.TrimSpace(p)
		}
		// TODO: should return first non-local address
		return parts[0]
	}
	return hdrRealIp
}

func GetUserAgent(r *http.Request) string {
	ua := r.UserAgent() //<---- simpler and faster!
	return ua
}

func GetWording(word_key string) string {
	return Wording[word_key]
}

func InTimeSpan(start string, end string, check time.Time) bool {
	loc, _ := time.LoadLocation(os.Getenv("TZ"))
	startDatetime, errStart := time.ParseInLocation("2006-01-02 15:04:05", start, loc)
	endDatetime, errEnd := time.ParseInLocation("2006-01-02 15:04:05", end, loc)
	if errStart == nil && errEnd == nil {
		return check.After(startDatetime) && check.Before(endDatetime)
	}

	log.Println(errStart)
	log.Println(errEnd)
	return false
}

func ChangeStringToDatetime(datetime string) time.Time {
	loc, _ := time.LoadLocation(os.Getenv("TZ"))
	dt, err := time.ParseInLocation("2006-01-02 15:04:05", datetime, loc)
	if err == nil {
		return dt
	}

	log.Println(err)
	return dt
}

func ChangeDatetimeToTimestamp(datetime time.Time) int64 {
	return datetime.Unix()
}

func CheckDuplicateSlice(data []interface{}) bool{

	for _, i := range data {
		irt:=0
		for _,i_2 := range data{
			if(i==i_2){
				irt++
				if(irt>=2){
					return false
					break
				}
			}
		}
    }
	return true

}

func UnixToTime(t_unix int64) time.Time{
	tm := time.Unix(t_unix, 0)
	log.Print(tm)
	return tm
}
