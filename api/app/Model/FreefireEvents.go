package Model

import (
	"log"
	"os"
	"context"
	"time"

	database "../../common/database"
	Constant "../Constant"

	// "gopkg.in/mgo.v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type FreefireEvents struct {
	Id                    primitive.ObjectID      `bson:"_id" json:"id"`
	Name                  string             `bson:"name" json:"name"`
	StartTime             string             `bson:"start_time" json:"start_time"`
	EndTime               string             `bson:"end_time" json:"end_time"`
	Status                string             `bson:"status" json:"status"`
	MailTitle		 	  string			 `bson:"mail_title" json:"mail_title"`
	MailContent		  	  string			 `bson:"mail_content" json:"mail_content"`
}

func GetEvents(eventname string) (FreefireEvents, string, bool) {
	var data FreefireEvents
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		return data, Constant.GetWording("cantconnectdb"), false
	}
	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection("freefire_events")

	// log.Print(col)
	filter := bson.M{"name": eventname}
	err := col.FindOne(context.Background(), filter).Decode(&data)
	if err != nil {
	    log.Fatal(err)
		return data, Constant.GetWording("nodata"), false
	}

	loc, _ := time.LoadLocation(os.Getenv("TZ"))
	n := time.Now().In(loc)
	openEvent := Constant.InTimeSpan(data.StartTime, data.EndTime, n)
	if openEvent == false {
		return data, Constant.GetWording("endEvent"), false
	}

	return data, Constant.GetWording("success"), true
}
