package Model

import (
	"log"
	"os"
	"time"
	"context"

	database "../../common/database"
	Constant "../Constant"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Users struct {
	Id                primitive.ObjectID   `bson:"_id" json:"id"`
	OpenId            string          `bson:"open_id" json:"open_id"`
	AccountId         int             `bson:"account_id" json:"account_id"`
	AccountName       string          `bson:"account_name" json:"account_name"`
	Platform          int             `bson:"platform" json:"platform"`
	PlatformId        int             `bson:"platform_id" json:"platform_id"`
	PlatformIcon      string          `bson:"platform_icon" json:"platform_icon"`
	Wrong			  int			  `bson:"wrong" json:"wrong"`
	Ban			  	  bool			  `bson:"ban" json:"ban"`
	BanAt			  int			  `bson:"ban_at" json:"ban_at"`
	BanTime			  int			  `bson:"ban_time" json:"ban_time"`
	Ip                string          `bson:"ip" json:"ip"`
	UserAgent         string          `bson:"useragent" json:"useragent"`
	CreatedAt         time.Time       `bson:"created_at" json:"created_at"`
	UpdatedAt         time.Time       `bson:"updated_at" json:"updated_at"`
}

func GetUserByOpenID(openId string) (Users, string, bool) {
	var data Users
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		return data, Constant.GetWording("cantconnectdb"), false
	}
	// defer mongo.Close()

	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME")+"_users")
	err := col.FindOne(context.Background(), bson.M{"open_id": openId}).Decode(&data)

	if err != nil {
		if err.Error() != Constant.GetWording("mongo_data_not_found") {
			log.Print("error while getting user event info: ", err)
			return data, Constant.GetWording("default"), false
		}
		return data, Constant.GetWording("nodata"), false
	}

	return data, Constant.GetWording("success"), true
}

func IsUniqueAccountId(accountId int) (Users, string, bool) {
	var data Users
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		return data, Constant.GetWording("cantconnectdb"), false
	}
	// defer mongo.Close()

	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME")+"_users")
	err := col.FindOne(context.Background(), bson.M{"account_id": accountId}).Decode(&data)
	if err != nil {
		if err.Error() != Constant.GetWording("mongo_data_not_found")  {
			log.Print("error while getting user unique account id: ", err)
			return data, Constant.GetWording("default"), false
		}
		return data, Constant.GetWording("success"), true
	}
	return data, Constant.GetWording("default"), false
}

func GetUserByOpenIDAndAccountID(openId string, accountId int) (Users, string, bool) {
	var data Users
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		return data, Constant.GetWording("cantconnectdb"), false
	}
	// defer mongo.Close()

	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME")+"_users")
	err := col.FindOne(context.Background(), bson.M{"open_id": openId, "account_id": accountId}).Decode(&data)
	if err != nil {
		if err.Error() != Constant.GetWording("mongo_data_not_found")  {
			log.Print("error while getting user event info: ", err)
			return data, Constant.GetWording("default"), false
		}
		return data, Constant.GetWording("nodata"), false
	}

	return data, Constant.GetWording("success"), true
}

func GetUserByAccountID(accountId int) (Users, string, bool) {
	var data Users
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		return data, Constant.GetWording("cantconnectdb"), false
	}
	// defer mongo.Close()

	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME")+"_users")
	err := col.FindOne(context.Background(), bson.M{"account_id": accountId}).Decode(&data)
	if err != nil {
		if err.Error() != Constant.GetWording("mongo_data_not_found")  {
			log.Print("error while getting user event info: ", err)
			return data, Constant.GetWording("default"), false
		}
		return data, Constant.GetWording("nodata"), false
	}

	return data, Constant.GetWording("success"), true
}

func InsertUser(m *Users) (Users, string, bool) {
	var data Users
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		return data, Constant.GetWording("cantconnectdb"), false
	}

	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME")+"_users")
	_, err := col.InsertOne(context.TODO(), m)

	if err != nil {
		if err.Error() != Constant.GetWording("mongo_data_not_found")  {
			log.Print("error while insert user event info: ", err)
			return data, Constant.GetWording("default"), false
		}
		return data, Constant.GetWording("nodata"), false
	}

	return data, Constant.GetWording("success"), true
}

func UpdateUser(colQuerier bson.M, change bson.M) (Users, string, bool) {
	var data Users
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		return data, Constant.GetWording("cantconnectdb"), false
	}
	// defer mongo.Close()

	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME")+"_users")
	_, err := col.UpdateOne(context.TODO(),colQuerier, change)
	// log.Print("result update :",result)
	if err != nil {
		log.Print("error while update user event info: ", err)
		return data, Constant.GetWording("nodata"), false
	}

	return data, Constant.GetWording("success"), true
}
