package Model

import (
	"time"
	"os"
	"context"
	"log"
	database "../../common/database"
	Constant "../Constant"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type SendLogs struct {
	Id              primitive.ObjectID `bson:"_id" json:"id"`
	OpenId          string        `bson:"open_id" json:"open_id"`
	AccountId       int           `bson:"account_id" json:"account_id"`
	AccountName     string        `bson:"account_name" json:"account_name"`
	Code   			string        `bson:"code" json:"code"`
	ItemList		Items	  	  `bson:"item_list" json:"item_list"`
	Status          string        `bson:"status" json:"status"`
	Ip              string        `bson:"ip" json:"ip"`
	UserAgent       string        `bson:"useragent" json:"useragent"`
	CreatedAtString string        `bson:"created_at_string" json:"created_at_string"`
	CreatedAtStringDT string      `bson:"created_at_string_dt" json:"created_at_string_dt"`
	CreatedAt       time.Time     `bson:"created_at" json:"created_at"`
	UpdatedAt       time.Time     `bson:"updated_at" json:"updated_at"`
}

type History struct {
	ItemList			Items	   `bson:"item_list" json:"item_list"`
	CreatedAtStringDT	string     `bson:"created_at_string_dt" json:"created_at"`
}

func GetItemHistoryAll(colQuerier bson.M) ([]map[string]interface{}, string,bool) {
	var data_history_list []map[string]interface{}
	data := make([]SendLogs, 0)
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		return data_history_list, Constant.GetWording("cantconnectdb"),false
	}

	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME")+"_sendlogs")
	opts := options.Find().SetSort(bson.D{{"created_at", -1}})
	cursor,err := col.Find(context.TODO(),colQuerier,opts)
	if err != nil {
		log.Print("error|GetItemHistoryAll|", err)
		return data_history_list, Constant.GetWording("default")+" (11)",false
	}
	if err = cursor.All(context.TODO(), &data); err != nil {
		// log.Print(data)
	    log.Print(err)
		return data_history_list, Constant.GetWording("default")+" (22)",false
	}

	for _,item := range data{
		// for _,list := range item{
			data_item:=map[string]interface{}{
				"code":item.Code,
				"created_at":item.CreatedAtStringDT,
			}

			data_history_list=append(data_history_list,data_item)
		// }

	}

	return data_history_list, "",true
}
