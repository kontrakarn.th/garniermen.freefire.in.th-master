package Model

import (
	"log"
	"os"
	"time"
	"context"

	database "../../common/database"
	Constant "../Constant"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Code struct {
	Id                primitive.ObjectID   `bson:"_id" json:"id"`
	Code              string          `bson:"code" json:"code"`
	Prefix            string          `bson:"prefix" json:"prefix"`
	Postfix           string          `bson:"postfix" json:"postfix"`
	CodeType          string          `bson:"code_type" json:"code_type"`
	Used              bool            `bson:"used" json:"used"`
	UsedByAccountId   int             `bson:"used_by_account_id" json:"used_by_account_id"`
	UsedByName        string          `bson:"used_by_name" json:"used_by_name"`
	UsedAt            time.Time       `bson:"used_at" json:"used_at"`
	UsedAtString      string          `bson:"used_at_string" json:"used_at_string"`
	Ip                string          `bson:"ip" json:"ip"`
	UserAgent         string          `bson:"useragent" json:"useragent"`
}

func GetCode(code string) (Code, string, bool) {
	var data Code
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		return data, Constant.GetWording("cantconnectdb"), false
	}
	// defer mongo.Close()

	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME")+"_code")
	err := col.FindOne(context.Background(), bson.M{"code": code}).Decode(&data)

	if err != nil {
		if err.Error() != Constant.GetWording("mongo_data_not_found") {
			log.Print("error while getting code 1: ", err)
			return data, Constant.GetWording("dup_or_wrong"), false
		}
		return data, Constant.GetWording("dup_or_wrong"), false
	}

	return data, Constant.GetWording("success"), true
}

func UpdateCode(colQuerier bson.M, change bson.M) (Code, string, bool) {
	var data Code
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		return data, Constant.GetWording("cantconnectdb"), false
	}
	// defer mongo.Close()

	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME")+"_code")
	_, err := col.UpdateOne(context.TODO(),colQuerier, change)
	// log.Print("result update :",result)
	if err != nil {
		log.Print("error while update code 2: ", err)
		return data, Constant.GetWording("nodata"), false
	}

	return data, Constant.GetWording("success"), true
}
