package Model
//
import (
	// database "../../common/database"
	// Constant "../Constant"
	// "log"
	// "os"
	"time"
	// "context"
	//
	// "go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "go.mongodb.org/mongo-driver/mongo/options"
)

type Deduct struct {
	Id              primitive.ObjectID `bson:"_id" json:"id"`
	OpenId          string        `bson:"open_id" json:"open_id"`
	AccountId       int           `bson:"account_id" json:"account_id"`
	AccountName     string        `bson:"account_name" json:"account_name"`
	DeductType      string        `bson:"deduct_type" json:"deduct_type"`
	Deduct          int       	  `bson:"deduct" json:"deduct"`
	OldPoint        int       	  `bson:"old_point" json:"old_point"`
	GetPoint        int       	  `bson:"get_point" json:"get_point"`
	NewPoint        int       	  `bson:"new_point" json:"new_point"`
	Ip              string        `bson:"ip" json:"ip"`
	UserAgent       string        `bson:"useragent" json:"useragent"`
	CreatedAtString string        `bson:"created_at_string" json:"created_at_string"`
	CreatedAtStringDT string      `bson:"created_at_string_datetime" json:"created_at_string_datetime"`
	DeductStatus	string		  `bson:"deduct_status" json:"deduct_status"`
	CreatedAt       time.Time     `bson:"created_at" json:"created_at"`
	UpdatedAt       time.Time     `bson:"updated_at" json:"updated_at"`
}

//
// func GetDeductLogs(account_id int,item_no int) (Deduct, string, bool) {
// 	var data Deduct
// 	mongo := database.GetNewMongoSession("mongo")
// 	if mongo == nil {
//
// 		log.Print("cant connect db")
// 		return data, Constant.GetWording("cantconnectdb"), false
// 	}
//
// 	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME")+"_deduct")
// 	err := col.FindOne(context.Background(), bson.M{"account_id":account_id,"item_no":item_no}).Decode(&data)
// 	if err != nil {
// 		if err.Error() != Constant.GetWording("mongo_data_not_found") {
// 			log.Print("error while getting item no data: ", err)
// 		}
// 		return data, Constant.GetWording("nodata"), false
// 	}
//
// 	return data, Constant.GetWording("success"), true
// }
//
// func GetItemHistoryDeductAll(colQuerier bson.M) ([]map[string]interface{}, string,bool) {
// 	var data_history_list []map[string]interface{}
// 	data := make([]Deduct, 0)
// 	mongo := database.GetNewMongoSession("mongo")
// 	if mongo == nil {
// 		log.Print("cant connect db")
// 		return data_history_list, Constant.GetWording("cantconnectdb"),false
// 	}
//
// 	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME")+"_deduct")
// 	opts := options.Find().SetSort(bson.D{{"created_at", -1}})
// 	cursor,err := col.Find(context.TODO(),colQuerier,opts)
// 	if err != nil {
// 		log.Print("error|getHistoryDeductAll|", err)
// 		return data_history_list, Constant.GetWording("default")+" (11)",false
// 	}
// 	if err = cursor.All(context.TODO(), &data); err != nil {
// 		// log.Print(data)
// 	    log.Print(err)
// 		return data_history_list, Constant.GetWording("default")+" (22)",false
// 	}
//
// 	for _,item := range data{
//
// 		for _,list := range item.ItemList{
// 			data_item:=map[string]interface{}{
// 				"item_no":list.No,
// 				"item_image":list.Image,
// 				"item_name":list.Items[0].Name,
// 				"created_at":item.CreatedAtStringDT,
//
// 			}
//
// 			data_history_list=append(data_history_list,data_item)
// 		}
//
// 	}
//
// 	return data_history_list, "",true
// }
