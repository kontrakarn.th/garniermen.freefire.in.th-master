package Model

import (
	database "../../common/database"
	Constant "../Constant"
	"log"
	"os"
	"time"
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Visit struct {
	Id              primitive.ObjectID `bson:"_id" json:"id"`
	AccountId       int           `bson:"account_id" json:"account_id"`
	ReportDate      string        `bson:"report_date" json:"report_date"`
	CreatedAt       time.Time     `bson:"created_at" json:"created_at"`
	UpdatedAt       time.Time     `bson:"updated_at" json:"updated_at"`
}


func ReportVistEvent(account_id int){
	var data Visit
    mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		// return data, Constant.GetWording("cantconnectdb"), false
	}

    loc, _ := time.LoadLocation(os.Getenv("TZ"))
    n:=time.Now().In(loc)
    n_string:=n.Format("2006-01-02")

	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME")+"_visit")
	err := col.FindOne(context.Background(),bson.M{"report_date":n_string,"account_id": account_id}).Decode(&data)
	if err != nil {
		if err.Error() != Constant.GetWording("mongo_data_not_found") {
			log.Print("error while getting visit event info: ", err)
		}else{
			reportins:=&Visit{
	    		Id                : primitive.NewObjectID(),
	    		AccountId         : account_id,
	    		ReportDate        : n_string,
	    		CreatedAt         : n,
	    		UpdatedAt         : n,
	    	}

			_, err_ins := col.InsertOne(context.TODO(), reportins)
			// log.Print(res)
	    	// err_ins := col.Insert(reportins)
	    	if err_ins != nil {
	    		log.Print(err_ins)
	    	}
			ReportIncrement("unique_visit",1)
		}
	}

    return
}
