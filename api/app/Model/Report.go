package Model

import (
    database "../../common/database"
	"log"
	"os"
    "context"
    "go.mongodb.org/mongo-driver/mongo/options"
    "go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
    "time"
)

type Report struct {
	Id             primitive.ObjectID `bson:"_id" json:"id"`
	ReportDate     string        `bson:"report_date" json:"report_date"`
}

func ReportIncrement(field string,amount int){
    mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {

		log.Print("cant connect db")
		// return data, Constant.GetWording("cantconnectdb"), false
	}
    col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME")+"_report")
    loc, _ := time.LoadLocation(os.Getenv("TZ"))
    n:=time.Now().In(loc)
    n_string:=n.Format("2006-01-02")

    opts := options.Count().SetMaxTime(2 * time.Second)
    numdate,err_numdate := col.CountDocuments(context.TODO(),bson.M{"report_date": n_string},opts)

    if(err_numdate!=nil){
        log.Print(err_numdate)
    }

    if(numdate==0){
        reportins:=&Report{
    		Id                 : primitive.NewObjectID(),
    		ReportDate             : n_string,
    	}

    	// err_ins := col.Insert(reportins)
        _, err_ins := col.InsertOne(context.TODO(), reportins)
        // log.Print(res)
    	if err_ins != nil {
    		log.Print(err_ins)
    	}
    }

    opts_2 := options.FindOneAndUpdate().SetUpsert(true)
    filter := bson.M{"report_date": n_string}
    update := bson.M{"$inc": bson.M{field: amount}}
    var updatedDocument bson.M
    err := col.FindOneAndUpdate(context.TODO(), filter, update, opts_2).Decode(&updatedDocument)
    if(err!=nil){
        log.Print(err)
    }

    return
}
