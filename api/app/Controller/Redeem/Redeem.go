package Redeem

import (
	// "fmt"
	"log"
	"net/http"
	"os"
	"time"

	// "strconv"

	common "../../../common"
	database "../../../common/database"
	Constant "../../Constant"
	"../../Controller"
	model "../../Model"
	"context"

	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func Redeem(w http.ResponseWriter, r *http.Request) {
	userData := common.GetUserData(r, os.Getenv("APP_SECRET"))

	if userData == nil || userData.OpenId == "" {
		Constant.ResponseMessage(w, nil, Constant.GetWording("mustlogin"), false)
		return
	}

	req := Constant.FormJson(r.Body)
	if req == nil {
		log.Print("dont have post data")
		Constant.ResponseMessage(w, nil, Constant.GetWording("default")+" (1)", false)
		return
	}

	code:=req["code"].(string)
	if(code==""){
		log.Print("wrong code :",code)
		Constant.ResponseMessage(w, nil, Constant.GetWording("default")+" (2)", false)
		return
	}

	openId := userData.OpenId
  	// get event
	events, msg, status := model.GetEvents(os.Getenv("EVENT_NAME"))
	if status == false {
		Constant.ResponseMessage(w, nil, msg, false)
		return
	}

	userEventInfo, msg, status := model.GetUserByOpenID(openId)
	if status == false {
		Constant.ResponseMessage(w, nil, msg, false)
		return
	}

	if userEventInfo.Ban==true {
		ban_util:=int64(userEventInfo.BanAt+userEventInfo.BanTime)
		time_ban:=Constant.UnixToTime(ban_util)
		time_ban_string:=time_ban.Format("02/01/2006 15:04:05")
		Constant.ResponseMessage(w, nil, Constant.GetWording("ban_util")+time_ban_string, false)
		return
	}

	code_get,code_msg,code_stat := model.GetCode(code)
	if code_stat == false {
		go func(u_ro model.Users){
			UpdateWrong(u_ro)
		}(userEventInfo)
		Constant.ResponseMessage(w, nil, code_msg, false)
		return
	}

	if(code_get.Used==true){
		go func(u_ro model.Users){
			UpdateWrong(u_ro)
		}(userEventInfo)
		Constant.ResponseMessage(w, nil, Constant.GetWording("dup_or_wrong"), false)
		return
	}

	mongo := database.GetNewMongoSession("mongo")
    if mongo == nil {
        Constant.ResponseMessage(w, nil, Constant.GetWording("cantconnectdb"), false)
        return
    }

	loc, _ := time.LoadLocation(os.Getenv("TZ"))
	n := time.Now().In(loc)
	nformat:=n.Format("2006-01-02")
	//
	myip := Constant.GetIPAdress(r)
	useragent := Constant.GetUserAgent(r)

	col_code := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME")+"_code")

	go func(code_ro string, userEventInfo_ro model.Users,myip_ro string,useragent_ro string) {
			//update code used
			col_code.UpdateOne(context.TODO(),bson.M{"code": code_ro, "used": false},
			bson.M{"$set": bson.M{
				"used":     true,
				"used_by_account_id":   userEventInfo_ro.AccountId,
				"used_by_name":   userEventInfo_ro.AccountName,
				"used_at": time.Now().In(loc),
				"used_at_string": time.Now().In(loc).Format("2006-01-02 15:04:05"),
				"ip": myip_ro,
				"useragent": useragent_ro,
			}},)

	}(code, userEventInfo,myip,useragent)

	var item_get model.Items
	var item_msg string
	var item_stat bool
	if(code_get.CodeType=="special"){
		item_get,item_msg,item_stat = model.GetItemByNo(2)
	}else{
		item_get,item_msg,item_stat = model.GetItemByNo(1)
	}
	if item_stat == false {
		Constant.ResponseMessage(w, nil, item_msg, false)
		return
	}

	itemlist := []map[string]interface{}{}

	coins := 0
	gems := 0

	for _, item_data := range item_get.Items {
		if item_data.Type == "gold" {
			coins += item_data.Cnt
		} else if item_data.Type == "gems" {
			gems += item_data.Cnt
		} else {
			itemlist = append(itemlist, map[string]interface{}{
				"id":  item_data.Id,
				"cnt": item_data.Cnt,
			})
		}
	}

	ReturnItem := map[string]interface{}{
		"item_image":   item_get.Image,
		"item_image_2":   item_get.Image2,
		"item_name":  item_get.Items[0].Name,
		"item_no":    item_get.No,
	}

	go func(code_get_ro model.Code){
		model.ReportIncrement("total_redeem", 1)
		model.ReportIncrement("total_redeem_"+code_get_ro.CodeType, 1)
	}(code_get)

	col_sendlogs := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME")+"_sendlogs")

	//send item
	sendlogsins := &model.SendLogs{
		Id:              primitive.NewObjectID(),
		OpenId:          userEventInfo.OpenId,
		AccountId:       userEventInfo.AccountId,
		AccountName:     userEventInfo.AccountName,
		Code:   		 code,
		ItemList:		 item_get,
		Status:          "pending",
		Ip:              myip,
		UserAgent:       useragent,
		CreatedAtString: nformat,
		CreatedAtStringDT: n.Format("2006-01-02 15:04:05"),
		CreatedAt:       n,
		UpdatedAt:       n,
	}

	_, err_ins := col_sendlogs.InsertOne(context.TODO(), sendlogsins)

	if err_ins != nil {
		log.Print(err_ins)
		Constant.ResponseMessage(w, nil, Constant.GetWording("contactcs"), false)
		return
	} else {

		sendItemId := sendlogsins.Id

		go func(send_id primitive.ObjectID,userEventInfo_ro_2 model.Users,events_ro model.FreefireEvents, itemlist_ro []map[string]interface{}, coins_ro int, gems_ro int) {
				// send Item
				var datasend = map[string]interface{}{
					"account_id": userEventInfo_ro_2.AccountId,
					"title":      events_ro.MailTitle,
					"content":    events_ro.MailContent,
					"coins":      coins_ro,
					"gems":       gems_ro,
					"items":      itemlist_ro,
				}

				status, response, err := Controller.SendMails(datasend)

				if err != nil {
					log.Println("erorr|Play:SendMails|", err)
				}

				col_sendlogs.UpdateOne(context.TODO(),bson.M{"_id": send_id, "status": "pending"},
				bson.M{"$set": bson.M{
					"status":     status,
					"response":   response,
					"updated_at": time.Now().In(loc),
				}},)

		}(sendItemId,userEventInfo,events, itemlist, coins, gems)

	}

	//update
	query_user_where:=bson.M{"account_id": userEventInfo.AccountId}
	query_user_up:=bson.M{"$set": bson.M{
		"wrong":0,
		"ban_at":0,
		"ban_time":0,
		"ban":false,
	}}

	_,msg_user_up,stat_user_up:=model.UpdateUser(query_user_where,query_user_up)
	if stat_user_up == false {
		Constant.ResponseMessage(w, nil, msg_user_up, false)
		return
	}

    data := map[string]interface{}{
		"item_get": ReturnItem,
	}

	Constant.ResponseMessage(w, data, "", true)
	return

}

func UpdateWrong(user model.Users){
	mongo := database.GetNewMongoSession("mongo")
    if mongo == nil {
		log.Print(Constant.GetWording("cantconnectdb"))
        return
    }
	col_users := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME")+"_users")
	opts_2 := options.FindOneAndUpdate().SetUpsert(false)
    filter := bson.M{"account_id": user.AccountId}
	var update bson.M
	new_wrong:=user.Wrong+1
	if(new_wrong>5){
		//minute -> second
		loc, _ := time.LoadLocation(os.Getenv("TZ"))
		ban_at:=time.Now().In(loc).Unix()
		ban_time:=new_wrong*5*60

		update = bson.M{"$set": bson.M{"wrong": new_wrong,"ban":true,"ban_at":ban_at,"ban_time":ban_time}}
	}else{
		update = bson.M{"$inc": bson.M{"wrong": 1}}
	}
    var updatedDocument bson.M
    err := col_users.FindOneAndUpdate(context.TODO(), filter, update, opts_2).Decode(&updatedDocument)
    if(err!=nil){
        log.Print(err)
    }
}
