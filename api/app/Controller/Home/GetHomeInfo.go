package Home

import (
	"log"
	"net/http"
	// "bytes"
	// "encoding/json"
	// "strings"
	"os"

	// "sync"
	// "strconv"
	"time"

	// "time"
	common "../../../common"
	// database "../../../common/database"
	Constant "../../Constant"
	"../../Controller"
	model "../../Model"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func GetHomeInfo(w http.ResponseWriter, r *http.Request) {

	userData := common.GetUserData(r, os.Getenv("APP_SECRET"))
	if userData == nil || userData.OpenId == "" {
		Constant.ResponseMessage(w, nil, Constant.GetWording("mustlogin"), false)
		return
	}
	// get event
	events, msg, status := model.GetEvents(os.Getenv("EVENT_NAME"))
	if status == false {
		Constant.ResponseMessage(w, nil, msg, false)
		return
	}

	// mongo := database.GetNewMongoSession("mongo")
	// if mongo == nil {
	// 	Constant.ResponseMessage(w, nil, Constant.GetWording("cantconnectdb"), false)
	// 	return
	// }

	// get user event info
	// openId := "c05c8cfc2bd2989489f621993f2e7f2d"
	// userData.OpenId = "c05c8cfc2bd2989489f621993f2e7f2d"
	openId := userData.OpenId

	userEventInfo, msg, status := model.GetUserByOpenID(openId)

	myip := Constant.GetIPAdress(r)
	useragent := Constant.GetUserAgent(r)
	loc, _ := time.LoadLocation(os.Getenv("TZ"))
	n := time.Now().In(loc)

	if status == false && msg == Constant.GetWording("nodata") {

		// get userinfo
		userInfo, err := Controller.GetUserInfo(userData.OpenId)
		if err != "" {
			if err != Constant.GetWording("nocharacter"){
				w.WriteHeader(http.StatusNotImplemented)
			}
			ret_data:=map[string]interface{}{
				"modal_type": "cantplay",
			}
			Constant.ResponseMessage(w, ret_data, err, false)
			return
		}

		if userInfo["region"] != os.Getenv("LOCK_REGION") {
			ret_data:=map[string]interface{}{
				"modal_type": "cantplay",
			}
			Constant.ResponseMessage(w, ret_data, Constant.GetWording("cantplay"), false)
			return
		}

		accountId := int(userInfo["account_id"].(float64))
		userEventInfo, _, get_status := model.GetUserByAccountID(accountId)
		if(get_status==true && ((userData.OpenId != userEventInfo.OpenId) || (userData.Platform != userEventInfo.Platform))){
			query_user_where:=bson.M{"account_id": userEventInfo.AccountId}
			query_user_up:=bson.M{"$set": bson.M{
				"open_id" : userData.OpenId,
				"platform" : userData.Platform,
			}}
			_,msg_user_up,stat_user_up:=model.UpdateUser(query_user_where,query_user_up)
			if stat_user_up == false {
				log.Print("UpdateUser : GetHomeInfo 1")
				Constant.ResponseMessage(w, nil, msg_user_up, false)
				return
			}
		}else{
			// check duplicate account id
			_, msg, status := model.IsUniqueAccountId(accountId)
			if status == false {
				Constant.ResponseMessage(w, nil, msg, false)
				return
			}

			// insert new user
			insertUser := &model.Users{
				Id:                primitive.NewObjectID(),
				OpenId:            userData.OpenId,
				AccountId:         accountId,
				AccountName:       userInfo["nickname"].(string),
				Platform:          userData.Platform,
				PlatformId:        userData.Uid,
				PlatformIcon:      userData.Icon,
				Wrong:			   0,
				Ban:			   false,
				BanAt:		   	   0,
				BanTime:		   0,
				Ip:                myip,
				UserAgent:         useragent,
				CreatedAt:         n,
				UpdatedAt:         n,
			}

			_, msg, status = model.InsertUser(insertUser)
			if status == false {
				Constant.ResponseMessage(w, nil, msg, false)
				return
			}
			model.ReportIncrement("total_user", 1)
		}

	}else if(status==true){
		if((userData.OpenId != userEventInfo.OpenId) || (userData.Platform != userEventInfo.Platform)){
			query_user_where:=bson.M{"account_id": userEventInfo.AccountId}
			query_user_up:=bson.M{"$set": bson.M{
				"open_id" : userData.OpenId,
				"platform" : userData.Platform,
			}}
			_,msg_user_up,stat_user_up:=model.UpdateUser(query_user_where,query_user_up)
			if stat_user_up == false {
				log.Print("UpdateUser : GetHomeInfo 2")
				Constant.ResponseMessage(w, nil, msg_user_up, false)
				return
			}
		}
	}

	userEventInfo, msg, status = model.GetUserByOpenID(openId)
	if status == false {
		Constant.ResponseMessage(w, nil, Constant.GetWording("default") +" (1)", false)
		return
	}

	go func(user_ro model.Users){
		model.ReportVistEvent(user_ro.AccountId)
		Controller.CheckBan(user_ro)
	}(userEventInfo)

	data := map[string]interface{}{
		"account_id":   userEventInfo.AccountId,
		"account_name": userEventInfo.AccountName,
		"ban":userEventInfo.Ban,
		"end_at":events.EndTime,
	}

	Constant.ResponseMessage(w, data, "", true)
	return
}
