package Home
//
import (
    // "log"
	"net/http"
	"os"
	common "../../../common"
	Constant "../../Constant"
	// "../../Controller"
	model "../../Model"

	"go.mongodb.org/mongo-driver/bson"
)

func GetHistory(w http.ResponseWriter, r *http.Request) {
	userData := common.GetUserData(r, os.Getenv("APP_SECRET"))
	if userData == nil || userData.OpenId == "" {
		Constant.ResponseMessage(w, nil, Constant.GetWording("mustlogin"), false)
		return
	}
	// get event
	_, msg, status := model.GetEvents(os.Getenv("EVENT_NAME"))
	if status == false {
		Constant.ResponseMessage(w, nil, msg, false)
		return
	}

	// get user event info
	openId := userData.OpenId

	userEventInfo, msg, status := model.GetUserByOpenID(openId)
	if status == false {
		Constant.ResponseMessage(w, nil, Constant.GetWording("default") +" (1)", false)
		return
	}

	colQuerierItemHistory := bson.M{"account_id": userEventInfo.AccountId}
	itemHistory, msgItemHistory,statItemHistory := model.GetItemHistoryAll(colQuerierItemHistory)
	if statItemHistory == false && msgItemHistory!=Constant.GetWording("nodata") {
			Constant.ResponseMessage(w, nil, msgItemHistory, false)
			return
	}

	data := map[string]interface{}{
		"history":   itemHistory,
	}

	Constant.ResponseMessage(w, data, "", true)
	return
}
