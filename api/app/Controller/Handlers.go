package Controller

import (
	// "database/sql"
	// "encoding/json"
	// "math"
	"os"
	// "reflect"
	// "strings"
	"time"
	//
	// "github.com/gorilla/mux"
	// common "../../common"
	"log"

	// database "../../common/database"
	// Constant "../Constant"
	// utils "../../common/utils"
	"go.mongodb.org/mongo-driver/bson"
	model "../Model"

	"reflect"

	// "errors"
)

func InArray(val interface{}, array interface{}) (exists bool, index int) {
	exists = false
	index = -1

	switch reflect.TypeOf(array).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(array)

		for i := 0; i < s.Len(); i++ {
			if reflect.DeepEqual(val, s.Index(i).Interface()) == true {
				index = i
				exists = true
				return
			}
		}
	}

	return
}

func CheckBan(user model.Users){
	loc, _ := time.LoadLocation(os.Getenv("TZ"))
	n := time.Now().In(loc)
	n_unix:=n.Unix()

	time_check:=int64(user.BanAt+user.BanTime)
	if(n_unix>time_check){
		//update
		query_user_where:=bson.M{"account_id": user.AccountId}
		query_user_up:=bson.M{"$set": bson.M{
			"ban":false,
			// "wrong":0,
			// "ban_at":0,
			// "ban_time":0,
		}}
		_,_,stat_user_up:=model.UpdateUser(query_user_where,query_user_up)
		if stat_user_up == false {
			log.Print("update user false")
			return
		}
	}
}
