package main

import (
    "github.com/gorilla/mux"
    "./common/http"
    "./app"
)

var routes []http.Route

func initRoutes() {
    routes = append(app.GetRoutes())
}

func NewRouter() *mux.Router {
    initRoutes()

    router := mux.NewRouter().StrictSlash(true)
    for _, route := range routes {
        router.
            Methods(route.Method).
            Path(route.Pattern).
            Name(route.Name).
            Handler(route.HandlerFunc)
    }

    return router
}
