package http

import (
	"encoding/json"
	"net/http"
)

func JsonResponse(w http.ResponseWriter, response interface{}) {
    w.Header().Set("Content-type", "application/json; charset=UTF-8;");
		json.NewEncoder(w).Encode(response)
}
