package common

import (
  	"crypto/aes"
  	"crypto/cipher"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"log"
	"net/http"
	"strconv"
	"strings"
)

type UserData struct {
	Gender int `json:"gender"`
	Icon string `json:"icon"`
	Nickname string `json:"nickname"`
	Platform int `json:"platform"`
	Uid int `json:"uid"`
	Username string `json:"username"`
	OpenId string `json:"open_id"`
}

/**
 * Check authorization header and get user data (OAUTH)
 */
func GetUserData(r *http.Request, secret string) *UserData {
	a := r.Header.Get("Authorization")
	if a != "" && strings.HasPrefix(a, "Bearer ") {
		tokenString := strings.Replace(a, "Bearer ", "", 1)

		// check jwt format
		dotcnt := strings.Count(tokenString, ".")
		if dotcnt != 2 {
			return nil
		}

		// JWT validation
		// ignore err with _
		token, _ := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		    // Don't forget to validate the alg is what you expect:
		    if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
		        return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		    }

		    // hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		    return []byte(secret), nil
		})
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			if claims["uid"] == nil {
				return nil
			}
			uid := int(claims["uid"].(float64))
			gender := int(claims["gender"].(float64))
			icon := claims["icon"].(string)
			nickname := claims["nickname"].(string)
			platform := int(claims["platform"].(float64))
			var username string
			var openId string
			if claims["username"] != nil {
				username = claims["username"].(string)
			}
			if claims["open_id"] != nil {
				openId = claims["open_id"].(string)
			}
			// create user data
			user := UserData{gender, icon, nickname, platform, uid, username, openId}
			return &user
		}
	}
	return nil
}

type SsoUserData struct {
	Platform int
	Timestamp int
	Uid int
	Username string
}

// decrypt Garena sso session to user data
func DecryptSsoSession(appKey string, sessionKey string) *SsoUserData {

	s := hex2bin(sessionKey)
	a := hex2bin(appKey)

	if len(s) % 16 != 0 {
		return &SsoUserData{}
	}

	return decryptData(s, a)
}

func hex2bin(s string) []byte {
	ret, _ := hex.DecodeString(s)
	return ret
}

func decryptData(data []byte, key []byte) *SsoUserData {
  	block, err := aes.NewCipher(key)
  	if err != nil {
  		log.Print(err)
  		return nil
  	}

  	mode := cipher.NewCBCDecrypter(block, []byte("\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000")) // iv

  	// CryptBlocks can work in-place if the two arguments are the same.
  	mode.CryptBlocks(data, data)

	//rnd := string(data[0:3]) // 3 bytes
	platform, _ := strconv.ParseInt(string(data[3]), 10, 32) // 1 bytes
	timestamp := binary.LittleEndian.Uint32(data[4:8]) // 4 bytes
	uid := binary.LittleEndian.Uint32(data[8:12]) // 4 bytes
	username := string(data[12:28]) // 16 bytes

	return &SsoUserData{int(platform), int(timestamp), int(uid), username}
}

/**
 * Check authorization header and get user data (OAUTH)
 */
func GetUserDataByString(token string, secret string) *UserData {
	// a := r.Header.Get("Authorization")
	if token!="" {
		tokenString := token

		// check jwt format
		dotcnt := strings.Count(tokenString, ".")
		if dotcnt != 2 {
			return nil
		}

		// JWT validation
		// ignore err with _
		token, _ := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		    // Don't forget to validate the alg is what you expect:
		    if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
		        return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		    }

		    // hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		    return []byte(secret), nil
		})
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			if claims["uid"] == nil {
				return nil
			}
			uid := int(claims["uid"].(float64))
			gender := int(claims["gender"].(float64))
			icon := claims["icon"].(string)
			nickname := claims["nickname"].(string)
			platform := int(claims["platform"].(float64))
			var username string
			var openId string
			if claims["username"] != nil {
				username = claims["username"].(string)
			}
			if claims["open_id"] != nil {
				openId = claims["open_id"].(string)
			}

			// create user data
			user := UserData{gender, icon, nickname, platform, uid, username, openId}
			return &user
		}
	}
	return nil
}
