package database

//
// @author ggadv2
//

import (
	"encoding/json"
	"log"
	"time"
)

/**
 * Return data from Redis or get the data from getter and cache it
 *
 * @param key string Key for this cache
 * @param ttl int Time in seconds to cache
 * @param getter func() interface{} Function to get the data
 *
 * @return interface{}, error
 */
func CacheRemember(key string, ttl int, getter func() interface{}) (interface{}, error) {

	rdc := GetRedisClient("cache")
	val, redisErr := rdc.Get(key).Result()
	if redisErr != nil {
		data := getter()
		// encode the data as json
		b, encodeErr := json.Marshal(data)
		if encodeErr != nil {
			return nil, encodeErr
		}
		redisErr := rdc.Set(key, string(b), time.Duration(ttl)*time.Second).Err()
		if redisErr != nil {
			log.Print("error while setting redis value: ", redisErr)
		}
		return data, nil
	}
	var r interface{}
	// decode the json data
	decodeErr := json.Unmarshal([]byte(val), &r)
	if decodeErr != nil {
		return nil, decodeErr
	}
	return r, nil
}

func CacheGet(key string) (interface{}, error) {
	rdc := GetRedisClient("cache")
	val, redisErr := rdc.Get(key).Result()
	if redisErr != nil {
		return nil, redisErr
	}

	var r interface{}
	// decode the json data
	decodeErr := json.Unmarshal([]byte(val), &r)
	if decodeErr != nil {
		return nil, decodeErr
	}
	return r, nil
}

func CacheSet(key string, ttl int, data interface{}) error {

	rdc := GetRedisClient("cache")

	b, encodeErr := json.Marshal(data)
	if encodeErr != nil {
		return encodeErr
	}

	if ttl > 0 {
		redisErr := rdc.Set(key, string(b), time.Duration(ttl)*time.Second).Err()
		if redisErr != nil {
			log.Print("error while setting redis value: ", redisErr)
		}
		return nil
	} else {
		redisErr := rdc.Set(key, string(b), -1).Err()
		if redisErr != nil {
			log.Print("error while setting redis value: ", redisErr)
		}
		return nil
	}
}

func CacheIncrementFloat(key string, value float64) error {
	rdc := GetRedisClient("cache")

	redisErr := rdc.IncrByFloat(key, value).Err()
	// redisErr := rdc.Set(key, string(b), time.Duration(ttl)*time.Second).Err()
	if redisErr != nil {
		log.Print("error while setting redis value: ", redisErr)
	}
	return nil
}
