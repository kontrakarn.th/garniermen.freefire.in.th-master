package database

import (
	"database/sql"
	"log"
	"github.com/go-redis/redis"
	_ "github.com/go-sql-driver/mysql"
	// "gopkg.in/mgo.v2"
    "os"
	"strconv"
	"sync"
	"time"
	"context"
	"go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type DatabaseConfig struct {
	Name string
	Driver string
	Host string
	Port string
	Database string
	Username string
	Password string
}

type DatabaseConfigs []DatabaseConfig

var databases sync.Map

func SetDbConfig(configs DatabaseConfigs) {
	for k, _ := range configs {
		d := configs[k]
    	databases.Store(d.Name, &d)
    }
}

func getConfig(dbname string) *DatabaseConfig {
	result, ok := databases.Load(dbname)
	if !ok {
		return nil
	}
    return result.(*DatabaseConfig)
}

// Get mysql db instance of the specified config name
//
// @see https://github.com/go-sql-driver/mysql
//

type MySQLSession struct {
	Session *sql.DB
	LastActive int64
	CreatedAt int64
}

var mysqlSessions sync.Map

func GetConnection(dbname string) *sql.DB {

	config := getConfig(dbname)
	if config == nil {
		log.Print("db does not exist: " + dbname)
		return nil
	}

	now := time.Now().Unix()
	result, exists := mysqlSessions.Load(dbname)
	if exists {
		mysqlSession := result.(*MySQLSession)
		//timedout := now - mysqlSession.LastActive > 60 // destroy connection idle for over 1 min
		//stale := now - mysqlSession.CreatedAt > 300 // destroy persistent connection over 5 mins
		var err error
		err = mysqlSession.Session.Ping()
		//if timedout || stale || err != nil {
		if err != nil {
			go func() {
				defer func() {
					if r := recover(); r != nil {
						log.Print("mysql closing panic: ", r)
					}
				}()
				log.Print("closing because of mysql error: ", err)
				mysqlSession.Session.Close()
			}()
			mysqlSessions.Delete(dbname)
		} else {
			mysqlSession.LastActive = now
			return mysqlSession.Session
		}
	}

	db, err := sql.Open(config.Driver, config.Username + ":" + config.Password + "@tcp(" + config.Host + ":" + config.Port + ")/" + config.Database + "?timeout=15s")
	if err != nil {
		log.Print("error while connecting to db: ", err)
		return nil
	}

	// set max connections
 	_, b := os.LookupEnv("MYSQL_MAX_CONNS")
 	var maxConn int
 	if b {
		v, _ := strconv.Atoi(os.Getenv("MYSQL_MAX_CONNS"))
		maxConn = v
	} else {
		maxConn = 10
	}
	db.SetMaxIdleConns(0)
	db.SetMaxOpenConns(maxConn)

	mysqlSession := &MySQLSession{
    	db,
    	now,
    	now,
    }

    mysqlSessions.Store(dbname, mysqlSession)
    return db
}

type RedisSession struct {
	Session *redis.Client
	LastActive int64
	CreatedAt int64
}

var redisSessions = map[string]*RedisSession{}

// Get instance of Redis client of the specified config name
//
// @see https://github.com/go-redis/redis
//
func GetRedisClient(dbname string) *redis.Client {

	config := getConfig(dbname)
	if config == nil {
		log.Print("db does not exist: " + dbname)
		return nil
	}

	now := time.Now().Unix()
	redisSession, exists := redisSessions[dbname]
	if exists {
		//timedout := now - redisSession.LastActive > 60 // destroy connection idle for over 1 min
		//stale := now - redisSession.CreatedAt > 300 // destroy persistent connection over 5 mins
		var err error
		_, err = redisSession.Session.Ping().Result()
		//if timedout || stale || err != nil {
		if err != nil {
			go func() {
				defer func() {
					if r := recover(); r != nil {
						log.Print("redis closing panic: ", r)
					}
				}()
				log.Print("closing because of redis error: ", err)
				redisSession.Session.Close()
			}()
			delete(redisSessions, dbname)
		} else {
			redisSession.LastActive = now
			return redisSession.Session
		}
	}

	d, _ := strconv.ParseInt(config.Database, 10, 32)
	client := redis.NewClient(&redis.Options{
		Addr: "garniermen-freefire-in-th_redis:" + config.Port,
		Password: "",
		DB: int(d),
		IdleTimeout: time.Duration(15) * time.Second,
	})
    redisSessions[dbname] = &RedisSession{
    	client,
    	now,
    	now,
    }
	return client
}

// Get Mongo session
//
// @see http://labix.org/mgo
//
// Example:
// c := session.DB("test").C("people")
// c.Insert(&Person{"Ale", "+55 53 8116 9639"}, &Person{"Cla", "+55 53 8402 8510"})
//
// result := Person{}
// err = c.Find(bson.M{"name": "Ale"}).One(&result)
//
//
// type MgoSession struct {
// 	Session *mgo.Session
// 	LastActive int64
// 	CreatedAt int64
// }
//
// var mgoSessions = map[string]*MgoSession{}
//
// func GetMongoSession(dbname string) *mgo.Session {
//
// 	config := getConfig(dbname)
// 	if config == nil {
// 		log.Print("db does not exist: " + dbname)
// 		return nil
// 	}
//
// 	now := time.Now().Unix()
// 	mgoSession, exists := mgoSessions[dbname]
// 	if exists {
// 		//timedout := now - mgoSession.LastActive > 60 // destroy connection idle for over 1 min
// 		//stale := now - mgoSession.CreatedAt > 300 // destroy persistent connection over 5 mins
// 		var err error
// 		err = mgoSession.Session.Ping()
// 		//if timedout || stale || err != nil {
// 		if err != nil {
// 			go func() {
// 				defer func() {
// 					if r := recover(); r != nil {
// 						log.Print("mongo closing panic: ", r)
// 					}
// 				}()
// 				log.Print("closing because of mongo error: ", err)
// 				mgoSession.Session.Close()
// 			}()
// 			delete(mgoSessions, dbname)
// 		} else {
// 			mgoSession.LastActive = now
// 			return mgoSession.Session
// 		}
// 	}
//
// 	// create new session
// 	var url string
// 	if len(config.Username) > 0 {
// 		url = "mongodb://" + config.Username + ":" + config.Password + "@" + config.Host + ":" + config.Port
// 	} else {
// 		url = "mongodb://" + config.Host + ":" + config.Port
// 	}
// 	session, err := mgo.DialWithTimeout(url, time.Duration(15) * time.Second)
//     if err != nil {
// 		log.Print("error while connecting to mongo name: " + dbname + " url: " + url + " error: ", err)
//     }
//     mgoSessions[dbname] = &MgoSession{
//     	session,
//     	now,
//     	now,
//     }
//     return session
// }

type NewMgoSession struct {
	Session *mongo.Client
	LastActive int64
	CreatedAt int64
}

var NewMgoSessions = map[string]*NewMgoSession{}

func GetNewMongoSession(dbname string) *mongo.Client{
	config := getConfig(dbname)
	if config == nil {
		log.Print("db does not exist: " + dbname)
		return nil
	}

	var url string
	if len(config.Username) > 0 {
		url = "mongodb://" + config.Username + ":" + config.Password + "@" + config.Host + ":" + config.Port
	} else {
		url = "mongodb://" + config.Host + ":" + config.Port
	}
	client, err := mongo.NewClient(options.Client().ApplyURI(url))
	ctx, _ := context.WithTimeout(context.Background(), time.Duration(15) * time.Second)

	now := time.Now().Unix()
	mgoSession, exists := NewMgoSessions[dbname]
	if exists {
		//timedout := now - mgoSession.LastActive > 60 // destroy connection idle for over 1 min
		//stale := now - mgoSession.CreatedAt > 300 // destroy persistent connection over 5 mins
		var err error
		err = mgoSession.Session.Ping(ctx, readpref.Primary())

		//if timedout || stale || err != nil {
		if err != nil {
			go func() {
				defer func() {
					if r := recover(); r != nil {
						log.Print("mongo closing panic: ", r)
					}
				}()
				log.Print("closing because of mongo error: ", err)
				// mgoSession.Session.Close()
			}()
			delete(NewMgoSessions, dbname)
		} else {
			mgoSession.LastActive = now
			return mgoSession.Session
		}
	}

	// create new session
	// log.Print(err)
	err = client.Connect(ctx)

    if err != nil {
		log.Print("error while connecting to mongo name: " + dbname + " url: " + url + " error: ", err)
    }

    NewMgoSessions[dbname] = &NewMgoSession{
    	client,
    	now,
    	now,
    }

	return client
}
